"""
Based on the article STABILIZED FINITE ELEMENT METHODS
L. P. Franca, G. Hauke, A. Masud

Advective-Diffusive Model
- k grad(u) + a . nabla(u) = f

with dirichlet boundary condition:

    u(0) = 0, u(1) = 1
"""

from dolfin import *
from mshr import *
import numpy as np

# PARAMETERS -------------------------------------------------------------------
kappa = 0.005  # Same as in the article
a = 1  # We assume the velocity field is just constant

# WORLD -----------------------------------------------------------------------
steps = 10

mesh = IntervalMesh(steps, 0, 1)  # Garlekin and with SUPG terms
E_mesh = IntervalMesh(steps*100, 0, 1)  # For exact solution

# FUNCTION SPACES -------------------------------------------------------------
V = FunctionSpace(mesh,   'CG', 1)
EV = FunctionSpace(E_mesh, 'CG', 1)  # for exact solution

# VARIATIONAL FORMULATION -----------------------------------------------------
kappa = Constant(kappa)
a = Constant(a)
f = Constant(0)

# Trial and test functions
v = TestFunction(V)
u = TrialFunction(V)

# Add SUPG stabilization terms
h = 1.0 / 10.0  # For some reason h = CellDiameter(mesh) fails.
vNorm = sqrt(a * a)

# Not able to calculate the inverse element. Look in references in the article.
# m  = 1.0 / 3.0 # * min{1/3, 2C_k}, C_k inverse estimate constant

# This makes sure we get 10.0 as pe number (exactly as in the article).
pe = h / (2.0 * kappa)  # We need to play around with this

def xi(val):
    newValue = val
    if float(val) > 1:
        newValue = Constant(1)

    return newValue

print("old pe  = ", float(pe))

pe = xi(pe)
tau = Constant(h / (2.0 * vNorm) * pe)

print("tau     = ", float(tau))
print("new pe  = ", float(pe))

S1 = tau * inner(-kappa * Dx(u, 0) + a * Dx(u, 0), a * Dx(v, 0)) * dx

# B == F
B = kappa * inner(grad(u), grad(v)) * dx + inner(a * Dx(u, 0), v) * dx
F = inner(f, v) * dx  # Note that f is not included in exact solution

# Boundary conditions
bc1 = DirichletBC(V, 0, "on_boundary && near(x[0], 0, DOLFIN_EPS)")
bc2 = DirichletBC(V, 1, "on_boundary && near(x[0], 1, DOLFIN_EPS)")
bc = [bc1, bc2]

# Compute solution
u = Function(V)
u_s1 = Function(V)
solve(B == F, u, bc)
solve((B + S1) == F, u_s1, bc)

# EXACT SOLUTION IN ONE DIMENSION ---------------------------------------------
h = Expression("(pow(e, (a * x[0]) / kappa ) - 1) \
 / (pow(e, a / kappa) - 1)", e=np.e, kappa=kappa, a=a, degree=2)

# h1 = project(h, EV)
h1 = Function(EV)
h1.interpolate(h)

exactFile = XDMFFile('solution/exact.xdmf')
xdmfile_u_s1 = XDMFFile('solution/u_s1.xdmf')
xdmfile_u = XDMFFile('solution/u.xdmf')
xdmfile_u.write(u)
xdmfile_u_s1.write(u_s1)
exactFile.write(h1)
