from dolfin import *
from mshr import *
import sympy as sp
import time
# With our without SUPG:
SUPG = True


k           = 1.0  # kappa
Vel         = (1.0, 0.0, 0.0)
numPolygons = 8
num_steps   = 50 # Time steps
T           = 5
dt          = T / num_steps


# DOMAIN
xmin = 0.0; ymin = 0.0
xmax = 1.0; ymax = 1.0
zmin = 0.0; zmax = 1.0

mesh = UnitCubeMesh(numPolygons, numPolygons, numPolygons)

# Function spaces
V = VectorElement("CG", mesh.ufl_cell(), 2)
Q = FiniteElement("CG", mesh.ufl_cell(), 1)

W = V * Q
VQ = FunctionSpace(mesh, W)

u_in = Constant( Vel )
noslip = Constant( (0.0, 0.0, 0.0) )

# BOUNDARY CONDITIONS

west = DirichletBC(VQ.sub(0), u_in,
        "on_boundary && near(x[0], %f)" % xmin)
        # "x[0] > %f && x[0] < %f" % (xmin, xmax / 10.0))

east = DirichletBC(VQ.sub(0), u_in,
        "on_boundary && near(x[0], %f)" % xmax)

bcs = [west, east]

(u, p) = TrialFunctions(VQ)
(v, q) = TestFunctions(VQ)

f = Constant((0.0, 0.0, 0.0))
a = inner(grad(u), grad(v))*dx + div(v)*p*dx + q*div(u)*dx
L = inner(f, v)*dx

# solution
U = Function(VQ)

solve(a == L, U, bcs)

u, p = U.split()

XDMFFile("./results/3dSolution_stokesflow.xdmf").write(u)
XDMFFile("./results/3dSolution_stokespressure.xdmf").write(p)


# ---------------------------------------------------------------------------------
# This is the AD equations


# pick max h from mesh
h = mesh.hmax()

# Peclet number
#m = sqrt(Vel[0] ** 2 + Vel[1] **2)
m = norm(u, 'l2')
Pe = m * h / (2.0 * k)

omega = sp.coth(Pe) - Pe ** (-1)

# Stabilisation parameter
tau = h / 2.0 * omega

print("Pe = %f, \n |Vel| / k = %f, \ntau = %f" % (Pe, m/k, tau))
time.sleep(1)


# FUNCTION SPACES
V = FunctionSpace(mesh, 'CG', 1)
W = VectorFunctionSpace(mesh, 'CG', 1)

c_0 = Expression("0.0", degree=2)


# BOUNDARY CONDITIONS
bc_west = DirichletBC(V, c_0, "on_boundary && near(x[0], %f)" % xmin)

bcs = [bc_west]

# Sauce
f = Expression('A * exp(-B * ( pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2) + pow(x[2] - 0.5, 2) )) < 0.0001 ? 0.0 : 0.1', A=0.005, B=1000.0, degree=2)
c_D = Expression("0.0", degree=1)


# Define our expressions used for the variational formulation
tau = Constant(tau)
Vel = interpolate(u, W)
k   = Constant(k)

c = TrialFunction(V)
v = TestFunction(V)
c_n = interpolate(c_D, V)
n = FacetNormal(mesh)

F = inner(v, (c - c_n) / dt) * dx + k * dot(grad(c), grad(v)) * dx + inner(dot(Vel, grad(c)), v) * dx - inner(f, v) * dx 

if SUPG:
    F += tau * inner(-k * div(grad(c)) + dot(Vel, grad(c)) - f, dot(Vel, grad(v))) * dx

a = lhs(F)
L = rhs(F)

# Solution
# WITH TIME
d = Function(V)
t = 0

VTKFile = File

if not SUPG:
    vtkfile = File('./results/3dsolution.pvd')
else:
    vtkfile = File('./results/3dsolution_SUPG.pvd')

for n in range(num_steps):
    t += dt
    #c_D.t = t
    solve(a == L, d, bcs)
    vtkfile << (d, t)
    c_n.assign(d)

print("Problem was solved with SUPG = %d" % SUPG)
