from dolfin import *
from mshr import *
import sympy as sp
"""
    Make sure if you want to run the code using SUPG or not below.
"""
SUPG = True


# -----------------------------------------------------------------------------------

k           = 0.015  # kappa
Vel         = 1.0
numPolygons = 10
num_steps   = 200 # Time steps
T           = 0.4
dt          = T / num_steps


# DOMAIN
mesh1 = UnitIntervalMesh(numPolygons)

# pick max h from mesh
h = 1.0 / numPolygons

# Peclet number
m = sqrt(Vel ** 2)
Pe = m * h / (2.0 * k)

omega = sp.coth(Pe) - Pe ** (-1)

# Stabilisation parameter
tau = h / 2.0 * omega

print("Pe = %f, \n |Vel| / k = %f, \ntau = %f" % (Pe, m/k, tau))

# FUNCTION SPACES
V = FunctionSpace(mesh1, 'CG', 1)
#W = FunctionSpace(mesh2, 'CG', 1)

# BOUNDARY CONDITIONS
bc1 = DirichletBC(V, 0.0,
        "on_boundary && near(x[0], 0.0, DOLFIN_EPS)")

bc2 = DirichletBC(V, 1.0,
        "on_boundary && near(x[0], 1.0, DOLFIN_EPS)")

bcs = [bc1, bc2]


c_0 = Expression("0.0", degree=1)
# Sauce
f = Expression('0.0', degree=1)
c_D = Expression("0.0", degree=1)

tau = Constant(tau)
Vel = Constant(Vel)
k   = Constant(k)

c = TrialFunction(V)
v = TestFunction(V)
c_n = interpolate(c_D, V)

F = inner(v, (c - c_n) / dt) * dx + k * c.dx(0) * v.dx(0) * dx + Vel * inner(c.dx(0), v) * dx - inner(f, v) * dx

if SUPG:
    """ 
        Note that c.dx(0).dx(0) is zero, because V is a function space of p.w. linear
        functions. 
    """
    F += tau * inner(-k * c.dx(0).dx(0) + Vel * c.dx(0) - f, Vel * v.dx(0)) * dx

a = lhs(F)
L = rhs(F)

# Solution
d = Function(V)
t = 0

VTKFile = File
if not SUPG:
    vtkfile = File('./results/1dsolution.pvd')
else:
    vtkfile = File('./results/1dsolution_SUPG.pvd')

for n in range(num_steps):
    t += dt
    solve(a == L, d, bcs)
    vtkfile << (d, t)
    c_n.assign(d)


print("Problem was solved with SUPG = %d" % SUPG)
