from dolfin import *
from mshr import *
import sympy as sp


k           = 0.05  # kappa
Vel         = (1.0, 0.0)
numPolygons = 64

# DOMAIN
xmin = 0.0; ymin = 0.0
xmax = 1.0; ymax = 1.0

house_x = [xmax / 3.0, xmax - xmax / 3.0]
house_y = [ymin, ymax / 4.0]

house = Rectangle(Point(house_x[0], house_y[0]), Point(house_x[1], house_y[1]))
domain = Rectangle(Point(xmin, ymin), Point(xmax, ymax)) - house

mesh = generate_mesh(domain, numPolygons)

# Function spaces

#V = VectorFunctionSpace(mesh, "CG", 2)
#Q = FunctionSpace(mesh, "CG", 1)
#W = MixedFunctionSpace([V, Q])

V = VectorElement("CG", mesh.ufl_cell(), 2)
Q = FiniteElement("CG", mesh.ufl_cell(), 1)

W = V * Q
VQ = FunctionSpace(mesh, W)

u_in = Constant( Vel )#Expression(("1.0", "0.0"), degree=1);
# u_in = Expression(("sin(x[1] * pi)", "0.0"), degree=2)

noslip = Constant( (0.0, 0.0) )

# BOUNDARY CONDITIONS

west = DirichletBC(VQ.sub(0), u_in,
        "on_boundary && near(x[0], %f)" % xmin)
        # "x[0] > %f && x[0] < %f" % (xmin, xmax / 10.0))

east = DirichletBC(VQ.sub(0), u_in,
        "on_boundary && near(x[0], %f)" % xmax)

north = DirichletBC(VQ.sub(0), u_in,
        "on_boundary && (near(x[1], %f))" % ymax)

south = DirichletBC(VQ.sub(0), noslip,
        "on_boundary && (near(x[1], %f))" % ymin)

bc_house = DirichletBC(VQ.sub(0), noslip,
    "(%f - DOLFIN_EPS < x[0] && x[0] < %f + DOLFIN_EPS) && (%f + DOLFIN_EPS < x[1] && x[1] < %f + DOLFIN_EPS)" % (house_x[0], house_x[1], house_y[0], house_y[1]))

bcs = [west, east, north, south, bc_house]

# variational problem
(u, p) = TrialFunctions(VQ)
(v, q) = TestFunctions(VQ)

f = Constant((0.0, 0.0))
a = inner(grad(u), grad(v))*dx + div(v)*p*dx + q*div(u)*dx
L = inner(f, v)*dx

# solution
U = Function(VQ)

solve(a == L, U, bcs)

u, p = U.split()

XDMFFile("results/2dSolution_stokesflow.xdmf").write(u)
XDMFFile("results/2dSolution_stokespressure.xdmf").write(p)


# ---------------------------------------------------------------------------------
# This is the AD equations
class WEST(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], xmin)

class EAST(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0], xmax)

class NORTH(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], ymax)

class SOUTH(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[1], ymin)

class HOUSE(SubDomain):
    def inside(self, x, on_boundary):
        return (house_x[0] - DOLFIN_EPS < x[0] and x[0] < house_x[1] + DOLFIN_EPS) and (house_y[0] + DOLFIN_EPS < x[1] and x[1] < house_y[1] + DOLFIN_EPS)
        


EAST = EAST()
NORTH = NORTH()
SOUTH = SOUTH()
WEST = WEST()
HOUSE = HOUSE()


boundaries = MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
WEST.mark(boundaries, 0)
NORTH.mark(boundaries, 1)
EAST.mark(boundaries, 2)
SOUTH.mark(boundaries, 3)
HOUSE.mark(boundaries, 4)

ds = Measure('ds', domain = mesh, subdomain_data = boundaries)

# pick max h from mesh
h = mesh.hmax()

# Peclet number
#m = sqrt(Vel[0] ** 2 + Vel[1] **2)
m = norm(u, 'l2')
Pe = m * h / (2.0 * k)

omega = sp.coth(Pe) - Pe ** (-1)

# Stabilisation parameter
tau = h / 2.0 * omega

print("Pe = %f, \n |Vel| / k = %f, \ntau = %f" % (Pe, m/k, tau))

# FUNCTION SPACES
V = FunctionSpace(mesh, 'CG', 1)
W = VectorFunctionSpace(mesh, 'CG', 1)

c_0 = Expression("0.0", degree=2)


# BOUNDARY CONDITIONS
bc_house = DirichletBC(V, "0.0",
    "(%f - DOLFIN_EPS < x[0] && x[0] < %f + DOLFIN_EPS) && (%f + DOLFIN_EPS < x[1] && x[1] < %f + DOLFIN_EPS)" % (house_x[0], house_x[1], house_y[0], house_y[1]))

bc_west = DirichletBC(V, c_0, WEST)

bcs = [bc_west]

# NEUMANN
g = Expression( ("0.0", "0.0"), degree=1)


# Define our expressions used for the variational formulation

# Sauce
f = Expression("A * exp(-B * ( pow(x[0] - 0.15, 2) + pow(x[1] - 0.25, 2) ))",A=0.02, B=100.0 ,degree=2)

tau = Constant(tau)
Vel = interpolate(u, W)
k   = Constant(k)

c = TrialFunction(V)
v = TestFunction(V)

n = FacetNormal(mesh)

#w = v + tau * dot(Vel, grad(v))
a = k * inner(grad(c), grad(v)) * dx + inner(dot(Vel, grad(c)), v) * dx
L = -inner(f, v) * dx + dot(g, n) * v * ds(0) + dot(g, n) * v * ds(1) + dot(g, n) * v * ds(2) + dot(g, n) * v * ds(3)
H = a + L
# SUPG TERM


b, M = lhs(H), rhs(H)
# SUPG TERM
H += tau * inner( -k * div(grad(c)) + dot(Vel, grad(c)) - f, dot(Vel, grad(v))) * dx
a, L = lhs(H), rhs(H)

# Solution
d = Function(V) # NO SUPG
solve(b == M, d, bcs)
e = Function(V) # WITH SUPG
solve(a == L, e, bcs)

XDMFFile("results/2dSolution.xdmf").write(d)
#XDMFFile("results/2dSolution_SUPG.xdmf").write(e)
