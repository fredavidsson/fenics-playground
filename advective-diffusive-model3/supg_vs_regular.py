from fenics import *
import sympy as sp

k           = 0.015  # kappa
Vel         = 1.0  # Constant velocity


# Number of steps
nx = 10

# stepping
h = 1.0 / nx

# Peclet number
Pe = Vel * h / (2.0 * k)

omega = sp.coth(Pe) - Pe ** (-1)

# Stabilisation parameter
tau = h / 2.0 * omega

print("Pe = %f, \nVel / k = %f, \ntau = %f" % (Pe, Vel/k, tau))

mesh1 = UnitIntervalMesh(nx)
mesh2 = UnitIntervalMesh(nx * 100)

# FUNCTION SPACES
V = FunctionSpace(mesh1, 'CG', 1)
W = FunctionSpace(mesh2, 'CG', 1)

# BOUNDARY CONDITIONS
bc1 = DirichletBC(V, 0.0,
        "on_boundary && near(x[0], 0.0, DOLFIN_EPS)")

bc2 = DirichletBC(V, 1.0,
        "on_boundary && near(x[0], 1.0, DOLFIN_EPS)")

bcs = [bc1, bc2]

# Define our expressions used for the variational formulation

Vel = Constant(Vel)
k   = Constant(k)
f   = Constant(0)
tau = Constant(tau)

u = TrialFunction(V)
v = TestFunction(V)
d = Function(W)

L = inner(k * u.dx(0), v.dx(0)) * dx + \
    inner(Vel * u.dx(0), v) * dx

a = inner(f, v) * dx # Apperently we get an error if we set this to 0.

# For SUPG
#H = L + tau * inner(-k * u.dx(0).dx(0) + Vel * u.dx(0), Vel * v.dx(0)) * dx

# Since we're in CG1, u'' = 0
H = L + tau * inner(Vel * u.dx(0), Vel * v.dx(0)) * dx


# Solution
c = Function(V) # NO SUPG
solve(L == a, c, bcs)
e = Function(V) # WITH SUPG
solve(H == a, e, bcs)

# exact solution
exact_solution = Expression(
    "1 / (exp(a / k) - 1) * (exp(a / k * x[0]) - 1)",
    a=Vel, k=k, degree=1)

d.interpolate(exact_solution)

XDMFFile("results/1DexactSolution.xdmf").write(d)
XDMFFile("results/1DwithoutSUPG.xdmf").write(c)
XDMFFile("results/1DwithSUPG.xdmf").write(e)
