from fenics import *
from mshr import *
import sympy as sp


k           = 0.5  # kappa
Vel         = (1.0, 0.0, 1.0)
numPolygons = 10

# DOMAIN
xmin = 0.0; ymin = 0.0
xmax = 1.0; ymax = 1.0
zmin = 0.0; zmax = 1.0

mesh = UnitCubeMesh(numPolygons, numPolygons, numPolygons)

#house_x = [xmax / 3.0, xmax - xmax / 3.0]
#house_y = [ymin, ymax / 4.0]

#house = Rectangle(Point(house_x[0], house_y[0]), Point(house_x[1], house_y[1]))

#domain = Rectangle(Point(xmin, ymin), Point(xmax, ymax)) - house

#mesh = generate_mesh(domain, numPolygons)

# Function spaces

#V = VectorFunctionSpace(mesh, "CG", 2)
#Q = FunctionSpace(mesh, "CG", 1)
#W = MixedFunctionSpace([V, Q])

V = VectorElement("CG", mesh.ufl_cell(), 2)
Q = FiniteElement("CG", mesh.ufl_cell(), 1)

W = V * Q
VQ = FunctionSpace(mesh, W)

u_in = Constant( Vel )#Expression(("1.0", "0.0"), degree=1);


noslip = Constant( (0.0, 0.0, 0.0) )

# BOUNDARY CONDITIONS
west = DirichletBC(VQ.sub(0), u_in,
        "on_boundary && near(x[0], %f)" % xmin)
bc2 = DirichletBC(VQ.sub(0), noslip,
        "on_boundary && near(x[1], %f)" % ymin)

bcs = [west]

# variational problem
(u, p) = TrialFunctions(VQ)
(v, q) = TestFunctions(VQ)

f = Constant((0.0, 0.0, 0.0))
a = inner(grad(u), grad(v))*dx + div(v)*p*dx + q*div(u)*dx
L = inner(f, v)*dx

# solution
U = Function(VQ)

solve(a == L, U, bcs)

u, p = U.split()

XDMFFile("results/3dSolution_stokesflow.xdmf").write(u)
XDMFFile("results/3dSolution_stokespressure.xdmf").write(p)
# pick max h from mesh
h = mesh.hmax()

# Peclet number
#m = sqrt(Vel[0] ** 2 + Vel[1] **2)
m = norm(u, 'l2')
Pe = m * h / (2.0 * k)

omega = sp.coth(Pe) - Pe ** (-1)

# Stabilisation parameter
tau = h / 2.0 * omega

print("Pe = %f, \n |Vel| / k = %f, \ntau = %f" % (Pe, m/k, tau))

# FUNCTION SPACES
V = FunctionSpace(mesh, 'CG', 1)
W = VectorFunctionSpace(mesh, 'CG', 1)

c_0 = Expression("0.0", degree=2)


# BOUNDARY CONDITIONS
bc_west = DirichletBC(V, c_0,
        "on_boundary && near(x[0], %f)" % xmin)

bcs = [bc_west]
# Define our expressions used for the variational formulation

# Sauce
f = Expression("A * exp(-B * ( pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2)  + pow(x[2] - 0.5, 2)))",A=0.02, B=100.0 ,degree=2)

tau = Constant(tau)
Vel = interpolate(u, W)
k   = Constant(k)

c = TrialFunction(V)
v = TestFunction(V)

n = FacetNormal(mesh)

w = v + tau * dot(Vel, grad(v))
L = k * inner(grad(c), grad(v)) * dx + inner(dot(Vel, grad(c)), v) * dx
a = inner(f, v) * dx  #+ dot(g, n) * v * ds #dot(n, g) * v * ds

# SUPG
H = inner( dot(  Vel  ,grad(c)) , w   ) * dx
H += inner(k * grad(c), grad(w)) *dx

# Solution
d = Function(V) # NO SUPG
solve(L == a, d, bcs)
e = Function(V) # WITH SUPG
solve(H == a, e, bcs)

XDMFFile("results/3dSolution.xdmf").write(d)
XDMFFile("results/3dSolution_SUPG.xdmf").write(e)
