from fenics import *
from mshr import *

k           = 10 ** (-1)  # kappa
Vel         = (0.0, 1.0)  # Constant velocity
numPolygons = 32


# DOMAIN
xmin = 0.0; ymin = 0.0
xmax = 20.0; ymax = 30.0

domain = Rectangle(Point(xmin, ymin), Point(xmax, ymax))
circ1  = Circle(Point(4.0, ymax - 4.0), 2.0)
circ2  = Circle(Point(xmax - 4.0, ymax / 2.0), 2.0)
rect1  = Rectangle(Point(xmin + 4.0, 1.0), Point(xmax - 2.0, 4.0))

domain = domain - circ1 - circ2 - rect1

mesh = generate_mesh(domain, numPolygons)

# Save the mesh for viewing in Paraview later
XDMFFile('results/mesh2D.xdmf').write(mesh)

# FUNCTION SPACES
V = FunctionSpace(mesh, 'CG', 1)
W = VectorFunctionSpace(mesh, 'CG', 1)

# BOUNDARY CONDITIONS
bc_x = DirichletBC(V, 0.0,
        "on_boundary && (near(x[0], %f) || near(x[0], %f))" % (xmin, xmax))
bc_y = DirichletBC(V, 0.0,
        "on_boundary && (near(x[1], %f) || near(x[1], %f))" % (ymin, ymax))

bc_circle = DirichletBC(V, -1.0,
        "on_boundary")

bcs = [bc_x, bc_y, bc_circle]

# Define our expressions used for the variational formulation

Vel = interpolate(Constant(Vel), W)
f   = Expression("1.0", degree=2)
k   = Constant(k)

u = TrialFunction(V)
v = TestFunction(V)

L = inner(k * grad(u), grad(v)) * dx + inner(div(Vel * u), v) * dx
a = inner(f, v) * dx

# Solution
c = Function(V)

solve(L == a, c, bcs)

XDMFFile("results/c2D.xdmf").write(c)
