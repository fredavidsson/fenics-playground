from fenics import *
from mshr import *

k           = 10 ** (-4)  # kappa
Vel         = (1.0)  # Constant velocity

# Number of steps
nx = 10;

# DOMAIN
xmin = 0.0;
xmax = 1.0;

mesh = UnitIntervalMesh(nx)

# FUNCTION SPACES
V = FunctionSpace(mesh, 'CG', 1)

# BOUNDARY CONDITIONS
bc1 = DirichletBC(V, 0.0,
        "on_boundary && near(x[0], %f)" % (xmin));

bc2 = DirichletBC(V, 0.0,
        "on_boundary && near(x[0], %f)" % (xmax));

bcs = [bc1, bc2]

# Define our expressions used for the variational formulation

Vel = Constant(Vel)
f   = Expression("1.0", degree=2)
k   = Constant(k)

u = TrialFunction(V)
v = TestFunction(V)

L = inner(k * Dx(u, 0), Dx(v, 0)) * dx + inner(Dx(Vel * u), v) * dx
a = inner(f, v) * dx

# Solution
c = Function(V)

solve(L == a, c, bcs)

XDMFFile("results/c1D.xdmf").write(c)
