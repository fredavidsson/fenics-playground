from dolfin import *
from mshr import *


numPolygons = 32

# DOMAIN
xmin = 0.0; ymin = 0.0
xmax = 1.0; ymax = 1.0

house_x = [xmax / 3.0, xmax - xmax / 3.0]
house_y = [ymin, ymax / 4.0]

house = Rectangle(Point(house_x[0], house_y[0]), Point(house_x[1], house_y[1]))

domain = Rectangle(Point(xmin, ymin), Point(xmax, ymax)) - house

mesh = generate_mesh(domain, numPolygons)

# Function spaces

#V = VectorFunctionSpace(mesh, "CG", 2)
#Q = FunctionSpace(mesh, "CG", 1)
#W = MixedFunctionSpace([V, Q])

V = VectorElement("CG", mesh.ufl_cell(), 2)
Q = FiniteElement("CG", mesh.ufl_cell(), 1)

W = V * Q
VQ = FunctionSpace(mesh, W)

u_in = Constant( (1.0, 0.0) )#Expression(("1.0", "0.0"), degree=1);
# u_in = Expression(("sin(x[1]*pi)", "0.0"), degree=2)

noslip = Constant( (0.0, 0.0) )

# BOUNDARY CONDITIONS
west = DirichletBC(VQ.sub(0), u_in,
        "on_boundary && near(x[0], %f)" % xmin)
        # "x[0] > %f && x[0] < %f" % (xmin, xmax / 10.0))

east = DirichletBC(VQ.sub(0), u_in,
        "on_boundary && near(x[0], %f)" % xmax)

north = DirichletBC(VQ.sub(0), u_in,
        "on_boundary && (near(x[1], %f))" % ymax)

south = DirichletBC(VQ.sub(0), noslip,
        "on_boundary && (near(x[1], %f))" % ymin)

bc_house = DirichletBC(VQ.sub(0), noslip,
    "(%f - DOLFIN_EPS < x[0] && x[0] < %f + DOLFIN_EPS) && (%f + DOLFIN_EPS < x[1] && x[1] < %f + DOLFIN_EPS)" % (house_x[0], house_x[1], house_y[0], house_y[1]));

bcs = [west, east, north, south, bc_house]

# variational problem
(u, p) = TrialFunctions(VQ)
(v, q) = TestFunctions(VQ)

f = Constant((0.0, 0.0))
a = inner(grad(u), grad(v))*dx + div(v)*p*dx + q*div(u)*dx
L = inner(f, v)*dx

# solution
U = Function(VQ)


solve(a == L, U, bcs);

u, p = U.split()

XDMFFile("results/2dSolution_stokesflow.xdmf").write(u)
XDMFFile("results/2dSolution_stokespressure.xdmf").write(p)
