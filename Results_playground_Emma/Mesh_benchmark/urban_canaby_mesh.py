import dolfin as dolfin
import mshr as mshr
import numpy as np

L = 50  # 50 mm
D = 100  # mm
H = [24, 48, 72, 96]  # different heights in mm

H = H[2]


def createBlock(x, y, height):
    # generates a mesh block with given height and coordinates
    X_1 = dolfin.Point(x, y, 0)
    X_2 = dolfin.Point(abs(x - L), abs(y - L), height)
    block = mshr.Box(X_1, X_2)
    return block


x_max = 24 * 1000
y_max = 3.0 * 1000
z_max = 2.2 * 1000

# y_max = 2.6 * 1000

blocks = []

for i in range(0, 30):
    i_even = i % 2 == 0
    j_0 = 0 if i_even else 1

    for j in range(0, j_0 + 14):
        x = i * D + 500
        y = j * D + (y_max / 2.0 - 15.0 / 2.0 * D) - j_0 * L + D
        block = createBlock(x, y, H)
        blocks.append(block)

min = dolfin.Point(0, 0, 0.0)
max = dolfin.Point(x_max, y_max, z_max)
box = mshr.Box(min, max)

s_w = 50      # Spire width
s_l = 250     # Spire length
s_h = 0.9 * 1000    # spire height

spires_distances = [(3.0 / 2.0 - 0.5) * 1000,
                    (3.0 / 2.0 - 1.0) * 1000,
                    (3.0 / 2.0) * 1000,
                    (3.0 / 2.0 + 0.5) * 1000,
                    (3.0 / 2.0 + 1.0) * 1000]
rectangles = []

for i in range(1, 6):
    y = spires_distances[i - 1] + s_w / 2.0
    x = x_max - (750 + s_l)

    X_1 = dolfin.Point(x, y, 0)
    X_2 = dolfin.Point(abs(x - s_l), abs(y - s_w), s_h)
    rectangle = mshr.Box(X_1, X_2)

    rectangles.append(rectangle)

s_w = 250    # Spire width
s_l = 50     # Spire length
s_h = 0.9 * 1000    # spire height


spires = []
for i in range(1, 6) :
    y = spires_distances[i - 1] + s_w / 2.0 - 120
    x = x_max - (1200 + s_l)

    origin = dolfin.Point(x, y, 0.0)

    box1_min = dolfin.Point(origin[0] - s_w, origin[1] - s_l, origin[2])
    box1_max = dolfin.Point(origin[0] + s_w , origin[1] + s_l, origin[2] - s_h)

    box2_min = dolfin.Point(origin[0], origin[1] + s_l, origin[2])
    box2_max = dolfin.Point(origin[0] - s_w , origin[1] - s_l, origin[2] + s_h)

    ellips_oid = mshr.Ellipsoid(origin, s_w, s_l, s_h, 32)
    spire = ellips_oid - mshr.Box(box1_min, box1_max) - mshr.Box(box2_min, box2_max)

    spires.append(spire)

# spires_ellips = []
#
# for i in range(1, 6) :
#
#     # y = spires_distances[i - 1] + s_w / 2.0
#     # x = x_max - (750 + s_l)
#     y = spires_distances[i - 1] + s_w / 2.0 - 120
#     x = x_max - (1200 + s_l)
#
#     origin = dolfin.Point(x, y, 0.0)
#
#     box1_min = dolfin.Point(origin[0] - s_w, origin[1] - s_l, origin[2])
#     box1_max = dolfin.Point(origin[0] + s_w , origin[1] + s_l, origin[2] - s_h)
#
#     box2_min = dolfin.Point(origin[0], origin[1] + s_l, origin[2])
#     box2_max = dolfin.Point(origin[0] - s_w , origin[1] - s_l, origin[2] + s_h)
#
#     ellips_oid = mshr.Ellipsoid(origin, s_w, s_l, s_h, 32)
#     spire = ellips_oid - mshr.Box(box1_min, box1_max) - mshr.Box(box2_min, box2_max)
#
#     spires_ellips.append(spire)


domain = blocks[0]
for i in range(1, len(blocks)):
    domain += blocks[i]

P_1 = dolfin.Point(x_max - 100, 0, 0)
P_2 = dolfin.Point(x_max - 140, y_max, 150)
plank = mshr.Box(P_1, P_2)

domain = domain + plank
objects = domain

for i in range(0, 5):
    domain += rectangles[i]
    # domain += spires_ellips[i]
    objects += spires[i]

domain = box - domain

domain_mesh = mshr.generate_mesh(domain, 36)
objects_mesh = mshr.generate_mesh(objects, 12)

dolfin.XDMFFile('urban_canopy_mesh.xdmf').write(domain_mesh)
dolfin.XDMFFile('urban_canopy_objects_rectangles.xdmf').write(objects_mesh)

# objects = box - objects     # NOTE : bug from here
#
# objects_mesh = mshr.generate_mesh(objects, 12)
# dolfin.XDMFFile('urban_canopy_objects_spires.xdmf').write(objects_mesh)
