import dolfin as dolfin
import mshr as mshr

s_w = 50    # Spire width
s_l = 250   # Spire length
s_h = 0.9 * 1000    # spire height

origin = dolfin.Point(0.0, 0.0, 0.0)

box1_min = dolfin.Point(origin[0] - s_w, origin[1] - s_l, origin[2])
box1_max = dolfin.Point(origin[0] + s_w , origin[1] + s_l, origin[2] - s_h)

box2_min = dolfin.Point(origin[0] - s_w, origin[1], origin[2])
box2_max = dolfin.Point(origin[0] + s_w , origin[1] + s_l, origin[2] + s_h)

ellips_oid = mshr.Ellipsoid(origin, s_w, s_l, s_h, 32)
spire = ellips_oid - mshr.Box(box1_min, box1_max) - mshr.Box(box2_min, box2_max)

# ellipsoid_mesh = mshr.generate_mesh(ellips_oid, 32)
spire1_mesh = mshr.generate_mesh(spire, 32)

# dolfin.XDMFFile('ellipsoid.xdmf').write(ellipsoid_mesh)
dolfin.XDMFFile('spire1.xdmf').write(spire1_mesh)


s_w = 250    # Spire width
s_l = 50     # Spire length
s_h = 0.9 * 1000    # spire height

box1_min = dolfin.Point(origin[0] - s_w, origin[1] - s_l, origin[2])
box1_max = dolfin.Point(origin[0] + s_w , origin[1] + s_l, origin[2] - s_h)

box2_min = dolfin.Point(origin[0], origin[1] + s_l, origin[2])
box2_max = dolfin.Point(origin[0] - s_w , origin[1] - s_l, origin[2] + s_h)

ellips_oid = mshr.Ellipsoid(origin, s_w, s_l, s_h, 32)
upper_ellipsoid = ellips_oid - mshr.Box(box1_min, box1_max)
spire = ellips_oid - mshr.Box(box1_min, box1_max) - mshr.Box(box2_min, box2_max)

spire2_mesh = mshr.generate_mesh(spire, 32)
dolfin.XDMFFile('spire2.xdmf').write(spire2_mesh)



#
