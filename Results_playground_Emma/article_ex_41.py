"""
    This is based on the article:
    Residual-based stabilized higer-order FEM for Advection-dominated problems.

    In the article the following is considered:
    for all a, alpha in L^infty(Omega), b vector-valued in H^1(omega)^d cap L^infty(Omega)^d
    and f square-integrable over L2(Omega).

    A(c, v) = (a nabla c, nabla v) + (b dot nabla c + alpha * c, v)
    l(v)    = (f, v)

    with (nabla dot b)(x) = 0, alpha(x) >= w >= 0, a(x) >= a_0 > 0    a.e. in Omega

    In example 4.1,
    a = 10 ** (-6), kappa = 10 ** (-4), h = 1/64 = 0.015625, alpha = 0

    So, A(c, v) = (a nabla c, nabla v) + (b dot nabla c, v)
        l(v)    = (f, v)

"""

from fenics import *

import ad_generator as AD_GEN
import mesh_generator as MESH_GEN
import stokes_generator as STOKES_GEN
import numpy as np
import csv

num_x = 16

SUPG = True # Change this for different effects

h = 1.0 / num_x

k = 10 ** (-6)

energy = []
poly = []
for p in range(1, 7):
    print("num_x = %d" % num_x)
    print("h = %f\t" % h)

    mesh = MESH_GEN.gen_article2_mesh(num_x) # generates the 2D mesh as in article
    energy.append(AD_GEN.solutionArticle2Example1(SUPG, mesh, p, h, k))
    poly.append(p)
    #xdmfFile.write(energy)

    print("\n-------------------------------------------------------------------\n")

#arr = [poly, energy]

#print(arr)

print(poly, energy)

with open('energy_3.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["polynomial", "energy"])

    for i in range(0, 6):
        writer.writerow([poly[i], np.log10(energy[i])])
