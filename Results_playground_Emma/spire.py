import dolfin as dolfin
import mshr as mshr

spire_min = dolfin.Point(0.0, 0.0, 0.0)
spire_max = dolfin.Point(300.0, 50.0, 0.9 * 1000)

radius = 1550

sphere_pos = dolfin.Point(-radius + 300.0, 50.0 / 2.0, 0.0)
sphere_1 = mshr.Sphere(sphere_pos, radius)

slice = mshr.Box(spire_min, spire_max) - sphere_1

plank = mshr.Box(spire_min, spire_max)

spire = plank - slice

#  rot_axis = dolfin.Point(0.0, 0.0, 1.0)
#  spire_2 = mshr.CSGRotation(spire, rot_axis, pi / 8)

domain = spire

mesh = mshr.generate_mesh(domain, 32)

sphere_mesh_1 = mshr.generate_mesh(sphere_1, 32)
dolfin.XDMFFile('spire.xdmf').write(mesh)
dolfin.XDMFFile('sphere_1.xdmf').write(sphere_mesh_1)
