import stokes_generator as Stokes
from mesh_generator import *
from sympy import coth
import math as Math

def solutionForArticle2(SUPG, mesh, savePath):
    """
        This function saves the solution to the given file path.

        SUPG: Can be given True or False
        mesh: The mesh created in mesh_generator
        savePath: Where the mesh file will be saved
    """
    # ----------------------------------------------------------------------
    # The advection-diffusive (AD) equations

    k = 10 ** (-6)  # Kappa (diffusive constant)

    # This is the velocity b in the article
    u = Constant( (1.0 / Math.sqrt(5), 2.0 / Math.sqrt(5)) )

    # SUPG terms, which are needed later
    h = mesh.hmax()
    m = 1.0 # because ||u|| = 1
    Pe = m * h / (2.0 * k)

    omega = coth(Pe) - Pe ** (-1)

    # Stabilisation parameter
    tau = h / 2.0 * omega

    # Function spaces
    V = FunctionSpace(mesh, 'CG', 1)

    # Boundary conditions
    bcs = DirichletBC(V, 0.0, "on_boundary")

    # Define our expressions used for the variational formulaton
    tau = Constant(tau)
    k = Constant(k)

    # I am so sorry
    #f = Expression("-tanh( (2 * x[0] - x[1] - 0.25) / sqrt(5 * a)) * pow( 2 / ( exp( (2 * x[0] - x[1] - 0.25) / sqrt(5*a) ) + exp( - (2 * x[0] - x[1] - 0.25)/sqrt(5 * a)) ), 2) + b_2 * pow(2 / (exp( (2 * x[0] - x[1] - 0.25)/sqrt(5*a) ) + exp( - (2 * x[0] - x[1] - 0.25)/sqrt(5*a) ) ), 2) / (2 * sqrt(5*a)) - b_1 * pow(2 / (exp( (2 * x[0] - x[1] - 0.25)/sqrt(5*a)) + exp(- (2 * x[0] - x[1] - 0.25)/sqrt(5*a)) ), 2) / (2 * sqrt(5 * a))", a=k, b_2 = 2.0 / Math.sqrt(5), b_1=1.0 / Math.sqrt(5), degree=1)
    """
    f = Expression(
        "- tanh( (2*x[0]-x[1] - 0.25)/sqrt(5*a) )" \
        "* pow( 1 / cosh( (2*x[0] - x[1] - 0.25) / sqrt(5 * a) ), 2 )" \
        "+ b_2 * pow( 1 / cosh( (2*x[0] - x[1] - 0.25) / sqrt(5 * a) ), 2 ) / (2*sqrt(5 * a))" \
        "- b_1 * pow( 1 / cosh( (2*x[0] - x[1] - 0.25) / sqrt(5 * a) ), 2 ) / (sqrt(5 * a))",
        a = k, b_1 = 1.0 / Math.sqrt(5), b_2 = 2.0 / Math.sqrt(5), degree=1)
    """

    f = Expression(
        "tanh((2 * x[0] - x[1] - 0.25)/sqrt(5*a))/pow(cosh( (2 * x[0] - x[1] - 0.25)/sqrt(5*a) ), 2)",
        a = k, degree=1)

    c = TrialFunction(V)
    v = TestFunction(V)

    A =  k * dot(grad(c), grad(v)) * dx + inner(dot(u, grad(c)), v) * dx
    L = inner(f, v) * dx

    if SUPG:
        A += tau * inner(-k * div(grad(c)) + dot(u, grad(c)), dot(u, grad(v))) * dx
        L += tau * inner(f, dot(u, grad(v))) * dx


    # Save the file
    vtkFile = File(savePath)

    # Solve problem
    C = Function(V)
    solve(A == L, C, bcs)

    vtkFile << C

    c_exact = Expression("0.5 * (1 - tanh( (2 * x[0] - x[1] - 0.25) / sqrt(5 * a) ))", a = k, degree=5)
    #c_exact = interpolate(c_exact, V)

    E = errornorm(c_exact, C)
    print("errornorm(c_exact, C) = %f" % E)
    E = norm(C, 'L2')
    print("||C|| = %f in L2 norm" % E)

    print("solution path\t= \"%s\"" % savePath)
    print("Pe \t\t= %f, \n|Vel| / k \t= %f, \ntau \t\t= %f" % (Pe, m/k, tau))

def solutionForArticle1(SUPG, mesh, savePath, a = 1.0):
    """
        This function saves the solution to the given file path.

        SUPG: Can be given True or False
        mesh: The mesh created in mesh_generator
        savePath: Where the mesh file will be saved
        a: is the constant velocity given in article
    """
        # ----------------------------------------------------------------------
    # The advection-diffusive (AD) equations

    alpha       = 0.01  # diffusive constant from the article
    num_steps   = 50 # Time steps
    T           = 1
    dt          = T / num_steps # Delta time

    # SUPG terms, which are needed later
    h = 0.02 # Spatal step size, given from article
    m = a # this is supposed to be calculated using the L2 norm.
    Pe = m * h / (2.0 * alpha)

    omega = coth(Pe) - Pe ** (-1)

    # Stabilisation parameter
    tau = h / 2.0 * omega


    # Function spaces
    V = FunctionSpace(mesh, 'CG', 2)

    t = 0.0

    # Boundary conditions
    u_t0 = Expression("exp(- pow((x[0] + 0.5), 2) / 0.00125 )", degree=1)
    u_0 = Expression("0.025 / sqrt(0.000625 + 0.02 * t) * exp(- pow(0.5 - t, 2) / (0.00125 + 0.04 * t) )", t = t, degree=1)
    u_1 = Expression("0.025 / sqrt(0.000625 + 0.02 * t) * exp(- pow(1.5 - t, 2) / (0.00125 + 0.04 * t) )", t = t, degree=1)

    g_0 = DirichletBC(V, u_0, "on_boundary && near(x[0], 0.0)")
    g_1 = DirichletBC(V, u_1, "on_boundary && near(x[0], 1.0)")

    bcs = [g_0, g_1]


    # Variational formulation
    u = TrialFunction(V)
    v = TestFunction(V)
    u_n = interpolate(Constant("0.0"), V)

    F = inner((u - u_n) / dt, v) * dx + inner(u, v) * dx - alpha * inner(u.dx(0).dx(0), v) * dx

    # SUPG part
    if SUPG:
        tau = Constant(tau)
        F += tau * inner(-alpha * u.dx(0).dx(0) + a * u.dx(0), a * v.dx(0)) * dx

    L, R = lhs(F), rhs(F)


    # Save the file
    vtkFile = File(savePath)

    # Solve problem
    C = Function(V)

    #solve(L == R, C, bcs)
    #vtkFile << C
    # Only display one certain type of error
    WARNING = 30
    set_log_level(WARNING)


    for n in range(num_steps):
        t += dt

        #progress = "\t%.2f %%" % ((t / T) * 100)
        #print(progress)

        solve(L == R, C, bcs)

        vtkFile << (C, t)

        u_0.t = t
        u_1.t = t

        u_n.assign(C)

    #print("SUPG \t\t= %s" % "True" if SUPG == True else "False")
    print("solution path\t= \"%s\"" % savePath)
    #print("Pe \t\t= %f, \n|Vel| / k \t= %f, \ntau \t\t= %f" % (Pe, m/k, tau))


def solutionFor3D(SUPG, mesh, savePath, u):
    """
        This function saves the solution to the given file path.

        SUPG: Can be given True or False
        mesh: The mesh created in mesh_generator
        savePath: Where the mesh file will be saved
        u: Is the velocity flow given by stokes
    """
        # ----------------------------------------------------------------------
    # The advection-diffusive (AD) equations

    k = 0.001  # Kappa (diffusive constant)
    num_steps   = 500 # Time steps
    T           = 5 # End time
    dt          = T / num_steps # Delta time

    # SUPG terms, which are needed later
    h = mesh.hmax()
    m = norm(u, 'l2')
    Pe = m * h / (2.0 * k)

    omega = coth(Pe) - Pe ** (-1)

    # Stabilisation parameter
    tau = h / 2.0 * omega


    # Function spaces
    V = FunctionSpace(mesh, 'CG', 1)
    W = VectorFunctionSpace(mesh, 'CG', 1)

    # Boundary conditions
    c_0 = Expression("0.0", degree=1)
    bottom = DirichletBC(V, c_0, "near(x[0], 0.0)")

    bcs = [bottom]

    # Source
    f = Expression('A * exp(-B * ( pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2) + pow(x[2] - 0.5, 2) )) < 0.0001 ? 0.0 : 0.1', A=0.005, B=1000.0, degree=2)

    # Define our expressions used for the variational formulaton
    tau = Constant(tau)
    a = interpolate(u, W)
    k = Constant(k)

    c = TrialFunction(V)
    v = TestFunction(V)
    c_n = interpolate(c_0, V)

    F = inner(v, (c - c_n) / dt) * dx + k * dot(grad(c), grad(v)) * dx + inner(dot(a, grad(c)), v) * dx - inner(f, v) * dx

    if SUPG:
        F += tau * inner(-k * div(grad(c)) + dot(a, grad(c)) - f, dot(a, grad(v))) * dx

    a, L = lhs(F), rhs(F)


    # Save the file
    vtkFile = File(savePath)

    # Solve problem
    C = Function(V)
    t = 0

    # Only display one certain type of error
    WARNING = 30
    set_log_level(WARNING)


    for n in range(num_steps):
        t += dt

        #progress = "\t%.2f %%" % ((t / T) * 100)
        #print(progress)

        solve(a == L, C, bcs)

        vtkFile << (C, t)

        c_n.assign(C)

    #print("SUPG \t\t= %s" % "True" if SUPG == True else "False")
    print("solution path\t= \"%s\"" % savePath)
    print("Pe \t\t= %f, \n|Vel| / k \t= %f, \ntau \t\t= %f" % (Pe, m/k, tau))

def solutionFor2D(SUPG, mesh, savePath, u):
    """
        This function saves the solution to the given file path.

        SUPG: Can be given True or False
        mesh: The mesh created in mesh_generator
        savePath: Where the mesh file will be saved
        u: Is the velocity flow given by stokes
    """
    # ----------------------------------------------------------------------
    # The advection-diffusive (AD) equations

    k = 0.00001  # Kappa (diffusive constant)
    num_steps   = 500 # Time steps
    T           = 5 # End time
    dt          = T / num_steps # Delta time

    # SUPG terms, which are needed later
    h = mesh.hmax()
    m = norm(u, 'l2')
    Pe = m * h / (2.0 * k)

    omega = coth(Pe) - Pe ** (-1)

    # Stabilisation parameter
    tau = h / 2.0 * omega


    # Function spaces
    V = FunctionSpace(mesh, 'CG', 1)
    W = VectorFunctionSpace(mesh, 'CG', 1)

    # Boundary conditions
    c_0 = Expression("0.0", degree=1)
    west = DirichletBC(V, c_0, "near(x[0], 0.0)")

    bcs = [west]

    # Source
    f = Expression('A * exp(-B * ( pow(x[0] - 0.1, 2) + pow(x[1] - 0.1, 2) )) < 0.0001 ? 0.0 : 0.1', A=0.005, B=1000.0, degree=2)

    # Define our expressions used for the variational formulaton
    tau = Constant(tau)
    a = interpolate(u, W)
    k = Constant(k)

    c = TrialFunction(V)
    v = TestFunction(V)
    c_n = interpolate(c_0, V)

    F = inner(v, (c - c_n) / dt) * dx + k * dot(grad(c), grad(v)) * dx + inner(dot(a, grad(c)), v) * dx - inner(f, v) * dx

    if SUPG:
        F += tau * inner(-k * div(grad(c)) + dot(a, grad(c)) - f, dot(a, grad(v))) * dx

    a, L = lhs(F), rhs(F)


    # Save the file
    vtkFile = File(savePath)

    # Solve problem
    C = Function(V)
    t = 0

    # Only display one certain type of error
    WARNING = 30
    set_log_level(WARNING)


    for n in range(num_steps):
        t += dt

        #progress = "\t%.2f %%" % ((t / T) * 100)
        #print(progress)

        solve(a == L, C, bcs)

        vtkFile << (C, t)

        c_n.assign(C)

    #print("SUPG \t\t= %s" % "True" if SUPG == True else "False")
    print("solution path\t= \"%s\"" % savePath)
    print("Pe \t\t= %f, \n|Vel| / k \t= %f, \ntau \t\t= %f" % (Pe, m/k, tau))
