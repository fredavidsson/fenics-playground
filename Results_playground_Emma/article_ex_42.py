from mshr import *
from fenics import *
from mpmath import coth
from mpmath import tanh
from math import pi
import ad_generator as AD_GEN
import numpy as np
import matplotlib.pyplot as plt
import csv

"""
    Example 4.2 from articel 'Residual - based stabilized higher - order FEM
    for advection - dominated problems' by G.Lube and G.Rapin

    SUPG : Can be given True or False
"""

# ------ The advection-diffusive (AD) parameters ------  #

k = 10**(-6)  # Kappa (diffusive constant)

# Velocity constant
u = Constant( (1.0  / sqrt(5), 2.0 / sqrt(5)) )

# Only display one certain type of error
WARNING = 30
set_log_level(WARNING)

slope_SUPG = []
slope_noSUPG = []

for poly_degree in range(1, 5) :              # We loop over the polynom degrees

    SUPG_norm_values   = []
    noSUPG_norm_values = []
    mesh_size          = []


    print(' ')
    print('############################################ ')
    print('poly_degree = %d' % poly_degree)

    for h_i in range(1, 7) :

        # Generate mesh
        mesh  = UnitSquareMesh(2 ** h_i, 2 ** h_i)
        h     = 2 ** (- h_i)

        mesh_size.append(h)

        # ----------- SUPG = True ----------- #

        C_SUPG, c_exact, _ = AD_GEN.solutionForArticle2Example2(True, mesh, poly_degree, h, k, u)

        L2_error_SUPG     = errornorm(c_exact, C_SUPG)


        SUPG_norm_values.append(L2_error_SUPG)

        # ----------- SUPG = False ----------- #
        C_noSUPG, _, _  = AD_GEN.solutionForArticle2Example2(False, mesh, poly_degree, h, k, u)

        L2_error_noSUPG = errornorm(c_exact, C_noSUPG)

        noSUPG_norm_values.append(L2_error_noSUPG)


    # Fit each polynomial degree
    polyfit_SUPG_coef   = np.polyfit(mesh_size, SUPG_norm_values, 1)
    polyfit_noSUPG_coef = np.polyfit(mesh_size, noSUPG_norm_values, 1)

    slope_SUPG.append(polyfit_SUPG_coef[0])
    slope_noSUPG.append( polyfit_noSUPG_coef[0])

    # print(polyfit_SUPG_coef, polyfit_noSUPG_coef)
    # print(' ')
    # print(polyfit_SUPG_coef[0], polyfit_noSUPG_coef[0])

print(slope_SUPG)
print(' ')
print(slope_noSUPG)






#
