from mshr import *
from fenics import *
from mpmath import coth
from mpmath import tanh
from math import pi
import ad_generator as AD_GEN
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc               # for Latex
import csv

"""
    Example 4.2 from articel 'Residual - based stabilized higher - order FEM
    for advection - dominated problems' by G.Lube and G.Rapin

    SUPG : Can be given True or False

    Code tailor customized for polynomial comparison

"""

# ------ The advection-diffusive (AD) parameters ------  #

k = 10**(-8)  # Kappa (diffusive constant)

# Velocity constant
u = Constant( (1.0  / sqrt(5), 2.0 / sqrt(5)) )


# Only display one certain type of error
WARNING = 30
set_log_level(WARNING)


fig_1, plt_1  = plt.subplots()
color         = ['b', 'g', 'r', 'k']

# --------------------- Plot all polynoms in on figure --------------------- #

for poly_degree in range(1, 5) :              # We loop over the polynom degrees

    norm_values = []
    norm_supg_values = []
    mesh_size   = []

    print(' ')
    print('############################################ ')
    print('poly_degree = %d' % poly_degree)

    for h_i in range(1, 7) :

        # Generate mesh
        mesh  = UnitSquareMesh(2 ** h_i, 2 ** h_i)
        h     = 2 ** (- h_i)

        C, c_exact, _ = AD_GEN.solutionForArticle2Example2(True, mesh, poly_degree, h, k, u)

        L2_error_norm = errornorm(c_exact, C)
        norm_supg_values.append(L2_error_norm)

        mesh_size.append(h)

        C, c_exact, _ = AD_GEN.solutionForArticle2Example2(False, mesh, poly_degree, h, k, u)

        L2_error_norm = errornorm(c_exact, C)

        norm_values.append(L2_error_norm)


    # Marker = "--o" if SUPG is True else "-s"
    label_1 = 'Polynom degree %d without SUPG' % poly_degree
    label_2 = 'Polynom degree %d with SUPG' % poly_degree

    # plt_1.plot(mesh_size, np.log10(norm_values), '--o', color = color[poly_degree - 1], label = label_1)

    # plt_1.plot(mesh_size, np.log10(norm_supg_values), '-s', color = color[poly_degree - 1], label = label_2)
    # plt_1.semilogy(mesh_size, norm_values, '--o')

    plt_1.loglog(mesh_size, norm_values, '--o',  color = color[poly_degree - 1], label = label_1)
    plt_1.loglog(mesh_size, norm_supg_values, '-s', color = color[poly_degree - 1], label = label_2)


# subplt_1.set_xlim(0.5, 1.0 / 64)
plt.rc('font', family = 'serif')
# fig_1.suptitle('Article_ex_4.2 results with and without SUPG \n when c_exact = 0.5*1 - tanh((2x - y - 0.25)/sqrt(5a)))')
fig_1.suptitle("Article_ex_4.2 results with and without SUPG \n"
               "when c_exact" r"= $ \frac{1}{2}(1 - \tanh( \frac{2x - y - \frac{1}{4}}{\sqrt{5a}} )  ) $")

# plt.xlabel('Mesh size')
plt.xlabel('log(Mesh size)')

# plt.ylabel('L2 error')
plt.ylabel('log(L2 error)')

plt.legend()
# plt.savefig('loglog_L2_error_results_article_ex_42')
# plt.show()



# --------------- Plot the polynom comparison in four figures --------------- #

fig_2, plt_2  = plt.subplots(2, 2)

nest_subplots = [plt_2[0,0], plt_2[0,1], plt_2[1,0], plt_2[1,1]]
SUPG_state    = [True, False]

for poly_degree in range(1, 5) :              # We loop over the polynom degrees

    SUPG_norm_values   = []
    noSUPG_norm_values = []

    print(' ')
    print('############################################ ')
    print('poly_degree = %d' % poly_degree)

    for info_SUPG in range(0, 2) :

        mesh_size = []

        for h_i in range(1, 7) :

            # Generate mesh
            mesh  = UnitSquareMesh(2 ** h_i, 2 ** h_i)
            h     = 2 ** (- h_i)

            mesh_size.append(h)

            if SUPG_state[info_SUPG] == True :

                # Solve AD
                C, c_exact, _ = AD_GEN.solutionForArticle2Example2(SUPG_state[info_SUPG], mesh, poly_degree, h, k, u)

                # Error norm
                L2_error_norm = errornorm(c_exact, C)

                SUPG_norm_values.append(L2_error_norm)

            else :

                # Solve AD
                C, c_exact, _ = AD_GEN.solutionForArticle2Example2(SUPG_state[info_SUPG], mesh, poly_degree, h, k, u)

                # Error norm
                L2_error_norm = errornorm(c_exact, C)

                noSUPG_norm_values.append(L2_error_norm)

        if SUPG_state[info_SUPG] == True :

            nest_subplots[poly_degree - 1].loglog(mesh_size, SUPG_norm_values, '--o', label = 'SUPG')
            # nest_subplots[poly_degree - 1].plot(mesh_size, np.log10(SUPG_norm_values), '--o', label = 'SUPG')


        else :

            nest_subplots[poly_degree - 1].loglog(mesh_size, noSUPG_norm_values, '--s', label = 'Without SUPG')
            # nest_subplots[poly_degree - 1].plot(mesh_size, np.log10(noSUPG_norm_values), '--s', label = 'Without SUPG')


        str_title = "Polynomial degree %d" % poly_degree
        nest_subplots[poly_degree - 1].set_title(str_title)
        nest_subplots[poly_degree - 1].legend()


        # ------------- End of info_SUPG loop ------------- #


# Create same x-axis and y-axis labels
for subplt in plt_2.flat :
    subplt.set( xlabel = 'log(Mesh size) \n', ylabel = 'log(L2 error)')
    # subplt.set( xlabel = 'Mesh size \n', ylabel = 'log(L2 error)')

# Generate head title
fig_2.suptitle('Article_ex_4.2 results when comparing each polynomial')

# plt.savefig('loglog_Poly_comparison_results_article_ex_42')
plt.show()



# ----------- Solve with and without SUPG for Paraview ----------- #

# Generate mesh
mesh  = UnitSquareMesh(64, 64)
h     = 1.0 / 64

C_SUPG, c_exact, c_exact_interpolated = AD_GEN.solutionForArticle2Example2(True, mesh, 2, h, k, u)
C_noSUPG, _, _ = AD_GEN.solutionForArticle2Example2(True, mesh, 2, h, k, u)


savePath_exact  = '/home/darebro/Documents/Master_Thesis/Results_playground/article_ex_42_exact.pvd'
pvdFile_exact   = File(savePath_exact)
savePath_SUPG   = '/home/darebro/Documents/Master_Thesis/Results_playground/article_ex_42_SUPG.pvd'
pvdFile_SUPG    = File(savePath_SUPG)

savePath_noSUPG = '/home/darebro/Documents/Master_Thesis/Results_playground/article_ex_42_noSUPG.pvd'
pvdFile_noSUPG  = File(savePath_noSUPG)

pvdFile_exact  << c_exact_interpolated
pvdFile_SUPG   << C_SUPG
pvdFile_noSUPG << C_noSUPG





























#
