from mshr import *
from fenics import *
from mpmath import coth
from mpmath import tanh
from math import pi
import csv

"""
    Example 4.2 from articel 'Residual - based stabilized higher - order FEM
    for advection - dominated problems' by G.Lube and G.Rapin

    SUPG : Can be given True or False
"""
# ------ The advection-diffusive (AD) parameters ------  #

k = 10**(-6)  # Kappa (diffusive constant)

# Velocity constant
u = Constant( (1.0  / sqrt(5), 2.0 / sqrt(5)) )

# SUPG terms, which are needed later
SUPG = True


# Only display one certain type of error
WARNING = 30
set_log_level(WARNING)


for poly_degree in range(1, 5) :              # We loop over the polynom degrees

    norm_values = []
    mesh_size   = []

    print(' ')
    print('############################################ ')
    print('poly_degree = %d' % poly_degree)

    for h_i in range(1, 7) :

        # Generate mesh
        mesh  = UnitSquareMesh(2 ** h_i, 2 ** h_i)
        h     = 2 ** (- h_i)


        # Function space
        V = FunctionSpace(mesh, 'CG', poly_degree)

        # Create SUPG parameters
        m     = 1.0            # Based on ||velocity||_p in article STABILIZED FINITE ELEMENT METHODS
        Pe    = float(m * h / (2.0 * k))
        omega = coth(Pe) - Pe ** (-1)

        # Stabilisation parameter
        tau   = h / 2.0 * omega


        print(' ')
        # NOTE : Need degree(f) << degree(c_exact) or else get warning messeges
        c_exact = Expression(" 0.5 * ( 1 - tanh( (2 * x[0] - x[1] - 0.25)/sqrt(5 * diff) ) ) ", diff = k, degree = poly_degree + 3)

        # Boundary conditions
        bcs = DirichletBC(V, c_exact, "on_boundary")

        # Define our expressions used for the variational formulaton
        tau = Constant(tau)
        k   = Constant(k)

        f = Expression(" - tanh( (2 * x[0] - x[1] - 0.25)/sqrt(5 * diff) )" \
                       " / pow( cosh((2 * x[0] - x[1] - 0.25)/sqrt(5 * diff)) , 2)", diff = k,  degree = poly_degree)              # Calculated by hand

        c = TrialFunction(V)
        v = TestFunction(V)

        A =  k * dot(grad(c), grad(v)) * dx + inner(dot(u, grad(c)), v) * dx
        L = inner(f, v) * dx

        if SUPG :
            A += tau * inner(-k * div(grad(c)) + dot(u, grad(c)), dot(u, grad(v))) * dx
            L += tau * inner(f, dot(u, grad(v))) * dx

        # Solve problem
        C = Function(V)
        solve(A == L, C, bcs)

        # ---------------- Error norms ---------------- #

        L2_norm    = errornorm(c_exact, C)
        norm_L2    = norm(C, 'L2')

        c_exact    = interpolate(c_exact, V)         # NOTE : Interpolate here

        print("mesh step               = %d" % h_i)
        print("L2_norm                 = %0.14f" % L2_norm)
        # print("||C||_{L2}              = %0.14f" % norm_L2)
        print(' ')

        #  Append norm

        mesh_size.append(h)
        norm_values.append(L2_norm)


    # Save plots for every polynom

    if SUPG == True :

        with open('data_%d_SUPG.csv' % poly_degree, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["mesh size", "L2 - error"])

            for i in range(0, 6):
                writer.writerow([mesh_size[i], norm_values[i]])

    else :

        with open('data_%d_noSUPG.csv' % poly_degree, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["mesh size", "L2 - error"])

            for i in range(0, 6):
                writer.writerow([mesh_size[i], norm_values[i]])




print('############################################ ')

print(' ')

if SUPG == True :
    print("SUPG                    = True")
else :
    print("SUPG                    = False")



# # ------------------------ Save the file ------------------------ #
# savePath_exact = '/home/darebro/Documents/Master_Thesis/Results_playground/solution_exact_article_ex_42.pvd'
# pvdFile_exact  = File(savePath_exact)
# savePath       = '/home/darebro/Documents/Master_Thesis/Results_playground/solution_article_ex_42.pvd'
# pvdFile        = File(savePath)
# pvdFile_exact << c_exact
# pvdFile << C
















#
