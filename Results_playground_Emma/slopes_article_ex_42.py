from mshr import *
from fenics import *
from mpmath import coth
from mpmath import tanh
from math import pi
import ad_generator as AD_GEN
import numpy as np
import matplotlib.pyplot as plt
import csv

"""
    Example 4.2 from articel 'Residual - based stabilized higher - order FEM
    for advection - dominated problems' by G.Lube and G.Rapin

    SUPG : Can be given True or False
"""

# ------ The advection-diffusive (AD) parameters ------  #
k_degree = 8
k        = 10**(- k_degree)  # Kappa (diffusive constant)

# Velocity constant
u = Constant( (1.0  / sqrt(5), 2.0 / sqrt(5)) )

# Only display one certain type of error
WARNING = 30
set_log_level(WARNING)

slope_SUPG = []
slope_noSUPG = []

for poly_degree in range(1, 5) :              # We loop over the polynom degrees

    norm_values        = []
    log_mesh_size      = []
    mesh_size          = []


    print(' ')
    print('############################################ ')
    print('poly_degree = %d' % poly_degree)

    for h_i in range(1, 7) :

        # Generate mesh
        mesh  = UnitSquareMesh(2 ** h_i, 2 ** h_i)
        h     = 2 ** (- h_i)

        log_mesh_size.append(np.log(h))
        mesh_size.append(h)

        # ----------- SUPG = True ----------- #

        C_SUPG, c_exact, _ = AD_GEN.solutionForArticle2Example2(True, mesh, poly_degree, h, k, u)

        L2_error_SUPG     = errornorm(c_exact, C_SUPG)


        norm_values.append(np.log(L2_error_SUPG))

    # Fit each polynomial degree
    polyfit_coef = np.polyfit(log_mesh_size[2 : 5], norm_values[2 : 5], 1)

    print('Polyfit at degree', poly_degree, '=', polyfit_coef[0])

print('')
print('Kappa = 10**(-%d)' % k_degree)




#
