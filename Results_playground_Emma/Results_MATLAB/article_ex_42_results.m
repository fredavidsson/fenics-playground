%% Plot results from csv - files

%%%%%%%%%%%%%%%%%%%% Note %%%%%%%%%%%%%%%%%%%%%
%                                             %
% Run the scritp cells in top to bottom order %
%                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath('/home/darebro/Documents/Master_Thesis/Results_playground/Results_files');

clc
clear all
clf
format short


%%%%%%%%%%%%%%%%%%%%% SUPG = True %%%%%%%%%%%%%%%%%%%%%

data_1_SUPG = csvread('data_1_SUPG.csv', 1, 0);

data_2_SUPG = csvread('data_2_SUPG.csv', 1, 0);

data_3_SUPG = csvread('data_3_SUPG.csv', 1, 0);

data_4_SUPG = csvread('data_4_SUPG.csv', 1, 0);

L2_error_poly_1_SUPG = data_1_SUPG(:, 2);
L2_error_poly_2_SUPG = data_2_SUPG(:, 2);
L2_error_poly_3_SUPG = data_3_SUPG(:, 2);
L2_error_poly_4_SUPG = data_4_SUPG(:, 2);

mesh_size       = data_1_SUPG(:, 1);

% Nested cell array
L2_error_SUPG = {L2_error_poly_1_SUPG, L2_error_poly_2_SUPG,...
    L2_error_poly_3_SUPG, L2_error_poly_4_SUPG};

[row, col] = size(L2_error_SUPG);

step       = [2, 4, 8, 16, 32, 64];


for plot_nr = 1 : 2
    subplot(2, 2, plot_nr)
    
    for cell_nr = 1 : col
        
        if plot_nr == 1
            
            plot(mesh_size, L2_error_SUPG{cell_nr}, '-o')
            hold on
            
        else
            
            plot(step, L2_error_SUPG{cell_nr}, '-o')
            hold on          
            
        end
        
    end
    
    if plot_nr == 1

        leg = legend('Polynomial degree 1', 'Polynomial degree 2',...
                     'Polynomial degree 3', 'Polynomial degree 4');
        
        set(leg, 'Location', 'northwest')   % Top left corner
        
        xlabel('Mesh size')
        ylabel('L2 error')
        
        title_str = sprintf('With SUGP');
        title(title_str)
        
    else
        
        legend('Polynomial degree 1', 'Polynomial degree 2',...
               'Polynomial degree 3', 'Polynomial degree 4' )
        
        xlabel('Number of mesh triangles')
        ylabel('L2 error')
        
        title_str = sprintf('With SUGP');
        title(title_str)
               
    end
        
        
    
    
end





%%%%%%%%%%%%%%%%%%%%% SUPG = False %%%%%%%%%%%%%%%%%%%%%

data_1_noSUPG = csvread('data_1_noSUPG.csv', 1, 0);

data_2_noSUPG = csvread('data_2_noSUPG.csv', 1, 0);

data_3_noSUPG = csvread('data_3_noSUPG.csv', 1, 0);

data_4_noSUPG = csvread('data_4_noSUPG.csv', 1, 0);

L2_error_poly_1_noSUPG = data_1_noSUPG(:, 2);
L2_error_poly_2_noSUPG = data_2_noSUPG(:, 2);
L2_error_poly_3_noSUPG = data_3_noSUPG(:, 2);
L2_error_poly_4_noSUPG = data_4_noSUPG(:, 2);

mesh_size              = data_1_noSUPG(:, 1);

% Nested cell array
L2_error_noSUPG = {L2_error_poly_1_noSUPG, L2_error_poly_2_noSUPG,...
    L2_error_poly_3_noSUPG, L2_error_poly_4_noSUPG};


[row, col] = size(L2_error_noSUPG);

step       = [2, 4, 8, 16, 32, 64];

figure(1)

for plot_nr = 3 : 4
    
    subplot(2, 2, plot_nr)
    
    for cell_nr = 1 : col
        
        if plot_nr == 3
            
            plot(mesh_size, L2_error_noSUPG{cell_nr}, '-o')
            hold on            
        else
            plot(step, L2_error_noSUPG{cell_nr}, '-o')
            hold on
        end
        
    end
    
    if plot_nr == 3
        
        leg = legend('Polynomial degree 1', 'Polynomial degree 2',...
                     'Polynomial degree 3', 'Polynomial degree 4');
        
        set(leg, 'Location', 'northwest')   % Top left corner
        
        xlabel('Mesh size')
        ylabel('L2 error')
        
        title_str = sprintf('Without SUGP');
        title(title_str)
        
        
        
    else
        
        legend('Polynomial degree 1', 'Polynomial degree 2',...
               'Polynomial degree 3', 'Polynomial degree 4' )
        
        xlabel('Number of mesh triangles')
        ylabel('L2 error')
        
        title_str = sprintf('Without SUGP');
        title(title_str)
        
        
    end
        
end




% ------- Text for clarification ------- %

% legend('Polynomial degree 1', 'Polynomial degree 2', 'Polynomial degree 3', 'Polynomial degree 4' )
% 
% title_str = sprintf('Without SUGP');
% title(title_str)
% 
% xlabel('Mesh size')
% ylabel('L2 error')

title_head = sprintf('L2 error norm with SUPG');
sgtitle(title_head)

%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Comparison with and without SUGP for the different         %
% polynomial degrees                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
clf(2)
figure(2)

L2_error_poly_1_comp = {L2_error_poly_1_SUPG, L2_error_poly_1_noSUPG};
L2_error_poly_2_comp = {L2_error_poly_2_SUPG, L2_error_poly_2_noSUPG};
L2_error_poly_3_comp = {L2_error_poly_3_SUPG, L2_error_poly_3_noSUPG};
L2_error_poly_4_comp = {L2_error_poly_4_SUPG, L2_error_poly_4_noSUPG};
 
L2_error_comp = {L2_error_poly_1_comp, L2_error_poly_2_comp,...
                 L2_error_poly_3_comp, L2_error_poly_4_comp};

[nbr_row, nbr_col] = size(L2_error_comp);
 
for plot_nr = 1 : nbr_col
   
    subplot(2, 2, plot_nr)
    L2_error = L2_error_comp{plot_nr};
   
    for k = 1:2
        
        plot(mesh_size, L2_error{k}, '-o')
        hold on
        xlabel('Mesh size')
        ylabel('L2 error')

%         plot(step, L2_error{k}, '-o')
%         hold on
%         xlabel('Number of triangles')
%         ylabel('L2 error')

        
    end
    
    title_str_c = sprintf('Polynimal degree %d', plot_nr);
    title(title_str_c)    

    leg = legend('SUPG', 'Without SUPG');
    
    set(leg, 'Location', 'northwest')   % Top left corner

    
end

sgtitle('Polynomial comparison')













