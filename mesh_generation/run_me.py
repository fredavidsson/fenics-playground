from fenics import *
from mshr import *
import numpy as np

# DOMAIN
xmin = 0.0; ymin = 0.0
xmax = 30.0; ymax = 40.0
numPolygons = 256

# Create a vector-valued function
mesh = generate_mesh(Rectangle(Point(xmin, ymin), Point(xmax, ymax)), numPolygons)
W = VectorFunctionSpace(mesh, 'P', 1)
w = Function(W)

# Get the coordinates for all the degrees of freedom
# and pick every other elements (u and v interleaved)
x = W.tabulate_dof_coordinates()[::2]

# Evaluate vector-valued function at the coordinates
u = -x[:,1]
v = x[:,0]

# Set values, making sure to interleave u and v
vals = np.empty((u.size + v.size,), dtype=u.dtype)
vals[0::2] = u
vals[1::2] = v
w.vector()[:] = vals

# Save to file for visualization
File('test.pvd') << w

# Save the coordinates
# print(x)
with open('mesh.csv', 'w') as f:
    f.write('\n'.join('%.16g,%.16g' % (coord[0], coord[1]) for coord in x))

"""
from fenics import *
from mshr import *

# DOMAIN
xmin = 0.0; ymin = 0.0
xmax = 30.0; ymax = 40.0
numPolygons = 256

mesh = generate_mesh(Rectangle(Point(xmin, ymin), Point(xmax, ymax)), numPolygons)


XDMFFile("results/2dMesh.xdmf").write(mesh)


# -----------

# Get the coordinates (NumNodes x 2 numpy array)
#mesh = UnitSquareMesh(2, 2)
coordinates = mesh.coordinates()
print(coordinates)
# Write to CSV file
with open('mesh.csv', 'w') as f:
    f.write('\n'.join('%.16g,%.16g' % (x[0], x[1]) for x in coordinates))
"""