"""
Based on:
    Petrov-Galerkin Formulations for Advection DiffusionEquation
    Dr. Cüneyt Sert (Finite element analysis in thermofluids, ch.6)

Advective-Diffusive Model
U d(T) - v d^2(T) = f in [0, L]

with dirichlet boundary condition:

    T(0) = T(L) = 0
"""
import dolfin as Fenics
import numpy as np
U = 1  # The constant velocity
f = 1  # known force function
L = 1
h = 0.1  # the discretization chosen as measure of mesh size
Pe = 0.25  # Change this for numerical tests
v = U * h / (2 * Pe)  # For different values of Pe
# -----------------------------------------------------------------------------

nx = int(L / h)  # nodes

# The mesh as in the article
mesh = Fenics.UnitIntervalMesh(nx)
mesh2 = Fenics.UnitIntervalMesh(nx * 100)

# Function spaces
V = Fenics.FunctionSpace(mesh, 'CG', 1)  # For numerical stabilization plots
V_EXACT = Fenics.FunctionSpace(mesh2, 'CG', 1)  # For exact solution


# Boundary conditions
bc1 = Fenics.DirichletBC(V, 0, "on_boundary && near(x[0], 0, DOLFIN_EPS)")
bc2 = Fenics.DirichletBC(V, 0, "on_boundary && near(x[0], %f, DOLFIN_EPS)" % L)
bc = [bc1, bc2]

# Define functions
T = Fenics.Function(V)
u = Fenics.TestFunction(V)
T_exact = Fenics.Function(V_EXACT)

# Define expressions used in variational forms
U = Fenics.Constant(U)
f = Fenics.Constant(f)
v = Fenics.Constant(v)

# Variational formulation
C = v * Fenics.inner(Fenics.Dx(T, 0), Fenics.Dx(u, 0)) * Fenics.dx
D = U * Fenics.inner(Fenics.Dx(T, 0), u) * Fenics.dx
F = Fenics.inner(f, u) * Fenics.dx

Fenics.solve(C + D - F == 0, T, bc)

# Exact solution
T_exact_expression = Fenics.Expression(
    "1.0 / U * ( x[0] - (1 - pow(e, U * x[0] / v)) / (1 - pow(e, U / v)) )",
    U=U, e=np.e, v=v, degree=1)

T_exact.interpolate(T_exact_expression)


xdmfile_T = Fenics.XDMFFile('solution_GFEM_1D/T.xdmf')
xdmfile_T_exact = Fenics.XDMFFile('solution_GFEM_1D/T_exact.xdmf')
xdmfile_T.write(T)
xdmfile_T_exact.write(T_exact)
