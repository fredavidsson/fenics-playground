"""
Based on:
    Petrov-Galerkin Formulations for Advection DiffusionEquation
    Dr. Cüneyt Sert (Finite element analysis in thermofluids, ch.6)

Advective-Diffusive Model
Vel * d(u) - eps * d^2(u) = f in [0, 1]

with dirichlet boundary condition:

    u(0) = u(1) = 0
"""
from dolfin import *
from mshr import *
import numpy as np

#Vel = (np.cos(np.pi / 4), np.sin(-np.pi / 4), 1)  # The constant velocity
Vel = (1, 0.1, 1)

eps = 10 ** (-4)
SUPG = True

# -----------------------------------------------------------------------------

#nx = 10
#mesh = UnitSquareMesh(nx, nx)
xmin = 0.0; ymin = 0.0; zmin = 0.0
xmax = 1.0; ymax = 1.0; zmax = 1.0
domain = Box(Point(xmin, ymin, zmin), Point(xmax, ymax, zmax)) - Box(Point(0.1, 0.1, 0.1), Point(0.6, 0.6, 0.6))

mesh = generate_mesh(domain, 32)

# Function spaces
W = VectorFunctionSpace(mesh, 'CG', 1)
V = FunctionSpace(mesh, 'CG', 1)

# Boundary conditions
bc1 = DirichletBC(V, 0,
    "on_boundary && near(x[0], %f)" % xmin)

#bc2 = DirichletBC(V, 0,
#    "on_boundary && near(x[1], %f)" % ymin)

bc = [bc1]

# Define expressions used in variational forms
Vel = interpolate(Constant(Vel), W)
f = Expression("1.0", degree=2)
eps = Constant(eps)

# Define functions
u = TrialFunction(V)
v = TestFunction(V)


# Crank-Nicolson
u0 = Constant(0.0)
u0 = interpolate(u0, V)
u_mid = 0.5 * (u0 + u)

F1 = ((u - u0) * v + inner(eps * grad(u_mid), grad(v)) \
   + dot(Vel, grad(u_mid)) * v - f * v) * dx

if SUPG:
    h = CellDiameter(mesh)
    tau = 0.5 * h / sqrt(dot(Vel, Vel))
    P = dot(Vel, grad(v))
    R = u - u0 + dot(Vel, grad(u_mid)) - div(eps * grad(u_mid)) - f

    F1 += tau * P * R * dx

a = lhs(F1)
L = rhs(F1)

A = assemble(a)
b = assemble(L)

for boundary in bc:
    boundary.apply(A)
    boundary.apply(b)


solver = LUSolver(A)

c = Function(V)
solver.solve(c.vector(), b)

xdmfile_c = XDMFFile('solution_GFEM_3D/c.xdmf')
xdmfile_c.write(c)
