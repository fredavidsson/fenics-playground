from fenics import *
from mshr import *


# DOMAIN
r = 0.10

domain = Rectangle(Point(0, 0), Point(1, 1))
circle = Circle(Point(0.5, 0.5), r)

domain = domain - circle
# GENERATE MESH
mesh = generate_mesh(domain, 24) # UnitSquareMesh(24)

XDMFFile('stokes/mesh.xdmf').write(mesh)

# SOLVE STOKES EQUATIONS TO GET VELOCITY FIELD
B = VectorElement("P", triangle, 2)
A = FiniteElement("P", triangle, 1)

TH = B * A
V = FunctionSpace(mesh, TH)

# BOUNDARY REGIONS
class InFlow(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] > 0.01

class NoslipBoundary(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary and (near(x[0], 0.0) or
                                near(x[0], 1.0) or
                                near(x[1], 0.0) or
                                near(x[1], 1.0))

# BOUNDARY CONDITIONS
inflow = InFlow()
noslipboundary = NoslipBoundary()

u_in = Expression(("1.0", "0.0"), degree=2)
bc_in = DirichletBC(V.sub(0), u_in, inflow)

noslip = Constant((0.0, 0.0))
bc_noslip = DirichletBC(V.sub(0), noslip, noslipboundary)

bcs_u = [bc_in]

# TRIAL AND TEST FUNCTIONS
(u, p) = TrialFunctions(V)
(v, q) = TestFunctions(V)

# VARIATIONAL FORMULATION
f = Expression(("0.0", "0.0"), degree=2)
a = inner(grad(u), grad(v))*dx + div(v)*p*dx + div(u)*q*dx
L = inner(f, v)*dx

# COMPUTE SOLUTION
c = Function(V)
solve(a == L, c, bcs_u)

(u, p) = c.split()

XDMFFile('stokes/u.xdmf').write(u)
XDMFFile('stokes/p.xdmf').write(p)
