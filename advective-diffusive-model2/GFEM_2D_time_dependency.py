"""
Based on:
    Petrov-Galerkin Formulations for Advection DiffusionEquation
    Dr. Cüneyt Sert (Finite element analysis in thermofluids, ch.6)

Advective-Diffusive Model
Vel * d(u) - eps * d^2(u) = f in [0, 1]

with dirichlet boundary condition:

    u(0, t) = u(1, t) = 0

Initial condition

    u(0, 0) = 1.0
"""
from dolfin import *
from mshr import *
import numpy as np

#Vel = (np.cos(np.pi / 4), np.sin(-np.pi / 4))
Vel = (1, 0.001)

# initial time t0, time length T, and time step ts.
t0 = 0; T = 3.0; ts = 0.01;

# Initial condition at t0
u0 = 0

eps = 10 ** (-4)
SUPG = False

# -----------------------------------------------------------------------------

time_interval = np.arange(t0 + ts, T, ts)
dt = Constant(ts)

nx = 10
mesh = UnitSquareMesh(nx, nx)

# Function spaces
W = VectorFunctionSpace(mesh, 'CG', 1)
V = FunctionSpace(mesh, 'CG', 1)

# Boundary conditions
bc1 = DirichletBC(V, 0,
    "on_boundary && (near(x[0], 0, DOLFIN_EPS) || near(x[1], 0, DOLFIN_EPS))")

bc2 = DirichletBC(V, 0,
    "on_boundary && (near(x[0], 1, DOLFIN_EPS) || near(x[1], 1, DOLFIN_EPS))")

bc = [bc1, bc2]

# Define expressions used in variational forms
Vel = interpolate(Constant(Vel), W)
f = Expression("sin(2 * pi * t) * sin(2 * pi * t)", t=0, degree=1)
eps = Constant(eps)

# Define functions
u = TrialFunction(V)
v = TestFunction(V)


# Crank-Nicolson with initial condition u0
u0 = Constant(u0)
u0 = interpolate(u0, V)
u_mid = 0.5 * (u0 + u)

F1 = ((u - u0) * v + dt * (inner(eps * grad(u_mid), grad(v)) \
   + dot(Vel, grad(u_mid)) * v - f*v)) * dx

if SUPG:
    h = CellDiameter(mesh)
    tau = 0.5 * h / sqrt(dot(Vel, Vel))
    P = dot(Vel, grad(v))
    R = u - u0 + dt * (dot(Vel, grad(u_mid)) - div(eps * grad(u_mid)) - f)

    F1 += tau * P * R * dx

a = lhs(F1)
L = rhs(F1)

A = assemble(a)
#b = assemble(L)

for boundary in bc:
    boundary.apply(A)
    #boundary.apply(b)


solver = LUSolver(A)

u = Function(V)

xdmfile_c = XDMFFile('solution_GFEM_2D_time_dependency/c.xdmf')
xdmfile_vel = XDMFFile('solution_GFEM_2D_time_dependency/Vel.xdmf')
xdmfile_vel.write(Vel)
t = 0
while t <= T:
    # Check Carl's code on why we use midpoint in time instead of CN.
    # He is using a more complex Expression source.
    t_mid = t - 0.5 * ts

    f.t = t_mid
    b = assemble(L)

    # Apply boundary conditions
    for boundary in bc:
        boundary.apply(b)

    solver.solve(u.vector(), b)
    xdmfile_c.write(u, t)

    # update previous solution
    u0.assign(u)
    t += float(ts)
    #print(str(t))
