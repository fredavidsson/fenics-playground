import fenics as fenics
import ad_generator as AD_GEN
import numpy as np
import matplotlib.pyplot as plt

from matplotlib import rc

# Only display one certain type of error
WARNING = 30
fenics.set_log_level(WARNING)


k = 0.015
u = 1.0

fig, ax = plt.subplots()
color = ['m', 'g', 'r', 'k', 'c']

for p in range(2, 7):
    print('################################')
    print("p = %d" % p)

    for SUPG in [True, False]:
        MESH_SIZE = []
        NORM_VALUES = []

        for i in range(1, 7):
            nx = 2 ** i
            mesh = fenics.UnitIntervalMesh(nx * 2)
            h = nx ** (-1)

            C, c_exact, _ = AD_GEN.solutionArticleLecture(SUPG, mesh, p, h, k, u)

            L2_error = fenics.errornorm(c_exact, C)

            MESH_SIZE.append(h)
            NORM_VALUES.append(L2_error)


        label = "Polynomial deg. %d with SUPG" if SUPG else "Polynomial deg. %d without SUPG"
        label = label % p
        marker = "-o" if SUPG else "--s"
        ax.loglog(MESH_SIZE, NORM_VALUES, marker, color = color[p - 2], label=label)

        if SUPG:
            poly = np.polyfit(np.log(MESH_SIZE[2:6]), np.log(NORM_VALUES[2:6]), 1)
            print(poly[0])


plt.rc('font', family="sans-serif")
rc('text', usetex=True)
fig.suptitle("Article test case with exact \n"
    r"$$c = \frac{1}{u} \big( x - \frac{ 1 - e^{u/\kappa} }{1 - e^{u/\kappa}} \big)$$")
ax.set(xlabel = r'$\log$(Mesh size)', ylabel = r'$\log$(L2 error)')
ax.legend()
plt.grid()
plt.show()

h = 0.1
mesh = UnitIntervalMesh(10)
p = 1
C, _, _ = AD_GEN.solutionArticleLecture(False, mesh, p, h, k, u)
C_SUPG, _, _ = AD_GEN.solutionArticleLecture(True, mesh, p, h, k, u)


mesh = UnitIntervalMesh(1000)
h = 1000 ** (-1)
_, _, c_exact_interpolated = AD_GEN.solutionArticleLecture(False, mesh, p, h, k, u)


fenics.XDMFFile("articleLecture_exact.xdmf").write(c_exact_interpolated)
fenics.XDMFFile("articleLecture_SUPG.xdmf").write(C_SUPG)
fenics.XDMFFile("articleLecture_noSUPG.xdmf").write(C)


#----

def solutionArticle2Example1(SUPG, mesh, p, h, k):
    # This is the velocity b in the article
    u = fenics.Expression(("-x[1]", "x[0]"), degree=p)

    # SUPG terms, which are needed later
    m = 1.0 # because ||u|| = 1
    Pe = m * h / (2.0 * k)

    omega = coth(Pe) - Pe ** (-1)

    # Stabilisation parameter
    tau = h / 2.0 * omega

    # Function space
    V = FunctionSpace(mesh, 'CG', p)

    # Boundary conditions
    bcs = DirichletBC(V, 0.0, "on_boundary")

    # Define our expressions used for the variational formulaton
    tau = Constant(tau)
    k = Constant(k)

    f = Expression(
        "a * exp(- x[0] * x[1]) * (sin(pi*x[1])*((- x[0]*x[0] - x[1]*x[1] + 2 * pi * pi) * sin(pi * x[0]) + 2 * pi * x[1] * cos(pi * x[0])) + 2 * pi * x[0] * sin(pi * x[0]) * cos(pi * x[1])) + x[0] * (pi * exp(- x[0] * x[1]) * sin( pi * x[0]) * cos( pi * x[1]) - x[0] * exp(- x[0] * x[1]) * sin(pi * x[0]) * sin( pi * x[1])) - x[1] * ( pi * exp( - x[0] * x[1] ) * cos(pi * x[0]) * sin(pi * x[1]) - x[1] * exp( - x[0] * x[1] ) * sin(pi * x[0]) * sin(pi * x[1]))",
        a = k,
        degree=(p + 3)
    )

    c = TrialFunction(V)
    v = TestFunction(V)

    A =  k * dot(grad(c), grad(v)) * dx + inner(dot(u, grad(c)), v) * dx
    L = inner(f, v) * dx

    if SUPG:
        A += tau * inner(-k * div(grad(c)) + dot(u, grad(c)), dot(u, grad(v))) * dx
        L += tau * inner(f, dot(u, grad(v))) * dx

    # Solve problem
    C = Function(V)
    solve(A == L, C, bcs)

    c_exact = Expression("sin( pi * x[0] ) * sin( pi * x[1] ) * exp(- x[0] * x[1])", a = k, degree=(p + 3))

    # E = (c_exact - C) ** 2 * dx
    # E = sqrt(abs(assemble(E)))
    # print("\nE \t= %f" % E)

    G = errornorm(c_exact, C)
    print("G \t= %f" % G)

    def article_err(C):
        energy = dot(sqrt(k) * (grad(C)), sqrt(k) * grad(C)) * dx + dot(C, C) * dx
        return sqrt(abs(assemble(energy)))
        #return sqrt(abs(assemble(energy)))

    #energy_article = article_err(C)
    #print("article's error-energy-norm \t= %f" % energy_article)

    c_e = interpolate(c_exact, V)
    energy = article_err(c_e - C)

    energy_format = sqrt(abs(energy))
    print("energy \t= {:.{}e}".format(energy_format, 8))

    C_L2 = norm(C, 'L2')

    c_exact = interpolate(c_exact, V)
    c_exact_L2 = norm(c_exact, 'L2')
    print("C_L2 \t\t= {:.{}f}".format(C_L2, 8))
    print("c_exact_L2 \t= {:.{}f}".format(c_exact_L2, 8))
    print("p \t\t= %d" % p)

    return energy


def solutionForArticle2Example2(SUPG, mesh, p, h, k, u):
    """
        This function saves the solution to the given file path.

        SUPG: Can be given True or False
        mesh: The mesh created in mesh_generator
        savePath: Where the mesh file will be saved
        p: this determines the polynomial degree of the space
    """
    # ----------------------------------------------------------------------
    # The advection-diffusive (AD) equations
    # Function space
    V = FunctionSpace(mesh, 'CG', p, h)

    # Create SUPG parameters
    m     = 1.0            # based on ||velocity||_p in article STABILIZED FINITE ELEMENT METHODS
    Pe    = float(m * h / (2.0 * k))

    #print("Peclet number Pe = %0.16f" % Pe)

    omega = coth(Pe) - Pe ** (-1)

    # Stabilisation parameter
    tau   = h / 2.0 * omega


    print(' ')
    c_exact = Expression(" 0.5 * ( 1 - tanh( (2 * x[0] - x[1] - 0.25)/sqrt(5 * diff) ) ) ", diff = k, degree = p + 3)

    # Boundary conditions
    bcs = DirichletBC(V, c_exact, "on_boundary")

    # Define our expressions used for the variational formulaton
    tau = Constant(tau)
    k   = Constant(k)

    # f = Expression(" - sinh( (2 * x[0] - x[1] - 0.25)/sqrt(5 * diff) )  / pow(cosh( (2 * x[0] - x[1] - 0.25)/sqrt(5 * diff) ), 3)", diff = k,  degree = i + 3)              # Calculated by hand
    f = Expression(" - tanh( (2 * x[0] - x[1] - 0.25)/sqrt(5 * diff) ) / pow( cosh((2 * x[0] - x[1] - 0.25)/sqrt(5 * diff)) , 2)", diff = k,  degree = p)              # Calculated by hand

    c = TrialFunction(V)
    v = TestFunction(V)

    A =  k * dot(grad(c), grad(v)) * dx + inner(dot(u, grad(c)), v) * dx
    L = inner(f, v) * dx

    if SUPG :
        A += tau * inner(-k * div(grad(c)) + dot(u, grad(c)), dot(u, grad(v))) * dx
        L += tau * inner(f, dot(u, grad(v))) * dx

    # Solve problem
    C = Function(V)
    solve(A == L, C, bcs)

    c_exact_interpolated = interpolate(c_exact, V)

    #  Append norm
    return C, c_exact, c_exact_interpolated

def solutionForArticle1(SUPG, mesh, savePath, a = 1.0):
    """
        This function saves the solution to the given file path.

        SUPG: Can be given True or False
        mesh: The mesh created in mesh_generator
        savePath: Where the mesh file will be saved
        a: is the constant velocity given in article
    """
        # ----------------------------------------------------------------------
    # The advection-diffusive (AD) equations

    alpha       = 0.01  # diffusive constant from the article
    num_steps   = 50 # Time steps
    T           = 1
    dt          = T / num_steps # Delta time

    # SUPG terms, which are needed later
    h = 0.02 # Spatal step size, given from article
    m = a # this is supposed to be calculated using the L2 norm.
    Pe = m * h / (2.0 * alpha)

    omega = coth(Pe) - Pe ** (-1)

    # Stabilisation parameter
    tau = h / 2.0 * omega


    # Function spaces
    V = FunctionSpace(mesh, 'CG', 2)

    t = 0.0

    # Boundary conditions
    u_t0 = Expression("exp(- pow((x[0] + 0.5), 2) / 0.00125 )", degree=1)
    u_0 = Expression("0.025 / sqrt(0.000625 + 0.02 * t) * exp(- pow(0.5 - t, 2) / (0.00125 + 0.04 * t) )", t = t, degree=1)
    u_1 = Expression("0.025 / sqrt(0.000625 + 0.02 * t) * exp(- pow(1.5 - t, 2) / (0.00125 + 0.04 * t) )", t = t, degree=1)

    g_0 = DirichletBC(V, u_0, "on_boundary && near(x[0], 0.0)")
    g_1 = DirichletBC(V, u_1, "on_boundary && near(x[0], 1.0)")

    bcs = [g_0, g_1]


    # Variational formulation
    u = TrialFunction(V)
    v = TestFunction(V)
    u_n = interpolate(Constant("0.0"), V)

    F = inner((u - u_n) / dt, v) * dx + inner(u, v) * dx - alpha * inner(u.dx(0).dx(0), v) * dx

    # SUPG part
    if SUPG:
        tau = Constant(tau)
        F += tau * inner(-alpha * u.dx(0).dx(0) + a * u.dx(0), a * v.dx(0)) * dx

    L, R = lhs(F), rhs(F)


    # Save the file
    vtkFile = File(savePath)

    # Solve problem
    C = Function(V)

    #solve(L == R, C, bcs)
    #vtkFile << C
    # Only display one certain type of error
    WARNING = 30
    set_log_level(WARNING)


    for n in range(num_steps):
        t += dt

        #progress = "\t%.2f %%" % ((t / T) * 100)
        #print(progress)

        solve(L == R, C, bcs)

        vtkFile << (C, t)

        u_0.t = t
        u_1.t = t

        u_n.assign(C)

    #print("SUPG \t\t= %s" % "True" if SUPG == True else "False")
    print("solution path\t= \"%s\"" % savePath)
    #print("Pe \t\t= %f, \n|Vel| / k \t= %f, \ntau \t\t= %f" % (Pe, m/k, tau))


def solutionFor3D(SUPG, mesh, savePath, u):
    """
        This function saves the solution to the given file path.

        SUPG: Can be given True or False
        mesh: The mesh created in mesh_generator
        savePath: Where the mesh file will be saved
        u: Is the velocity flow given by stokes
    """
        # ----------------------------------------------------------------------
    # The advection-diffusive (AD) equations

    k = 0.001  # Kappa (diffusive constant)
    num_steps   = 500 # Time steps
    T           = 5 # End time
    dt          = T / num_steps # Delta time

    # SUPG terms, which are needed later
    h = mesh.hmax()
    m = norm(u, 'l2')
    Pe = m * h / (2.0 * k)

    omega = coth(Pe) - Pe ** (-1)

    # Stabilisation parameter
    tau = h / 2.0 * omega


    # Function spaces
    V = FunctionSpace(mesh, 'CG', 1)
    W = VectorFunctionSpace(mesh, 'CG', 1)

    # Boundary conditions
    c_0 = Expression("0.0", degree=1)
    bottom = DirichletBC(V, c_0, "near(x[0], 0.0)")

    bcs = [bottom]

    # Source
    f = Expression('A * exp(-B * ( pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2) + pow(x[2] - 0.5, 2) )) < 0.0001 ? 0.0 : 0.1', A=0.005, B=1000.0, degree=2)

    # Define our expressions used for the variational formulaton
    tau = Constant(tau)
    a = interpolate(u, W)
    k = Constant(k)

    c = TrialFunction(V)
    v = TestFunction(V)
    c_n = interpolate(c_0, V)

    F = inner(v, (c - c_n) / dt) * dx + k * dot(grad(c), grad(v)) * dx + inner(dot(a, grad(c)), v) * dx - inner(f, v) * dx

    if SUPG:
        F += tau * inner(-k * div(grad(c)) + dot(a, grad(c)) - f, dot(a, grad(v))) * dx

    a, L = lhs(F), rhs(F)


    # Save the file
    vtkFile = File(savePath)

    # Solve problem
    C = Function(V)
    t = 0

    # Only display one certain type of error
    WARNING = 30
    set_log_level(WARNING)


    for n in range(num_steps):
        t += dt

        #progress = "\t%.2f %%" % ((t / T) * 100)
        #print(progress)

        solve(a == L, C, bcs)

        vtkFile << (C, t)

        c_n.assign(C)

    #print("SUPG \t\t= %s" % "True" if SUPG == True else "False")
    print("solution path\t= \"%s\"" % savePath)
    print("Pe \t\t= %f, \n|Vel| / k \t= %f, \ntau \t\t= %f" % (Pe, m/k, tau))

def solutionFor2D(SUPG, mesh, savePath, u):
    """
        This function saves the solution to the given file path.

        SUPG: Can be given True or False
        mesh: The mesh created in mesh_generator
        savePath: Where the mesh file will be saved
        u: Is the velocity flow given by stokes
    """
    # ----------------------------------------------------------------------
    # The advection-diffusive (AD) equations

    k = 0.00001  # Kappa (diffusive constant)
    num_steps   = 500 # Time steps
    T           = 5 # End time
    dt          = T / num_steps # Delta time

    # SUPG terms, which are needed later
    h = mesh.hmax()
    m = norm(u, 'l2')
    Pe = m * h / (2.0 * k)

    omega = coth(Pe) - Pe ** (-1)

    # Stabilisation parameter
    tau = h / 2.0 * omega


    # Function spaces
    V = FunctionSpace(mesh, 'CG', 1)
    W = VectorFunctionSpace(mesh, 'CG', 1)

    # Boundary conditions
    c_0 = Expression("0.0", degree=1)
    west = DirichletBC(V, c_0, "near(x[0], 0.0)")

    bcs = [west]

    # Source
    f = Expression('A * exp(-B * ( pow(x[0] - 0.1, 2) + pow(x[1] - 0.1, 2) )) < 0.0001 ? 0.0 : 0.1', A=0.005, B=1000.0, degree=2)

    # Define our expressions used for the variational formulaton
    tau = Constant(tau)
    a = interpolate(u, W)
    k = Constant(k)

    c = TrialFunction(V)
    v = TestFunction(V)
    c_n = interpolate(c_0, V)

    F = inner(v, (c - c_n) / dt) * dx + k * dot(grad(c), grad(v)) * dx + inner(dot(a, grad(c)), v) * dx - inner(f, v) * dx

    if SUPG:
        F += tau * inner(-k * div(grad(c)) + dot(a, grad(c)) - f, dot(a, grad(v))) * dx

    a, L = lhs(F), rhs(F)


    # Save the file
    vtkFile = File(savePath)

    # Solve problem
    C = Function(V)
    t = 0

    # Only display one certain type of error
    WARNING = 30
    set_log_level(WARNING)


    for n in range(num_steps):
        t += dt

        #progress = "\t%.2f %%" % ((t / T) * 100)
        #print(progress)

        solve(a == L, C, bcs)

        vtkFile << (C, t)

        c_n.assign(C)

    #print("SUPG \t\t= %s" % "True" if SUPG == True else "False")
    print("solution path\t= \"%s\"" % savePath)
    print("Pe \t\t= %f, \n|Vel| / k \t= %f, \ntau \t\t= %f" % (Pe, m/k, tau))

def solutionArticleLecture(SUPG, mesh, p, h, k, u):
    """
        This function saves the solution to the given file path.

        SUPG: Can be given True or False
        mesh: The mesh created in mesh_generator
        savePath: Where the mesh file will be saved
        p: this determines the polynomial degree of the space
    """
    # ----------------------------------------------------------------------
    # The advection-diffusive (AD) equations
    # Function spaces
    V = FunctionSpace(mesh, 'CG', p)

    # Create SUPG parameters
    m     = 1.0            # based on ||velocity||_p in article STABILIZED FINITE ELEMENT METHODS
    Pe    = float(m * h / (2.0 * k))


    # print("Peclet number Pe = %0.16f" % Pe)

    omega = coth(Pe) - Pe ** (-1)

    # Stabilisation parameter
    tau   = h / 2.0 * omega

    c_exact = Expression("1.0 / u *( x[0] - (1 - exp(u * x[0] / k)) / (1 - exp(u/k)) )", u=u, k=k, degree = p + 3)

    # Boundary conditions
    bcs = DirichletBC(V, 0.0, "on_boundary")

    # Define our expressions used for the variational formulaton
    tau = Constant(tau)
    k   = Constant(k)

    f = Expression("1.0", degree = p)

    c = TrialFunction(V)
    v = TestFunction(V)

    A =  k * inner(c.dx(0), v.dx(0)) * dx + inner(u * c.dx(0), v) * dx
    L = inner(f, v) * dx

    if SUPG :
        A += tau * inner(-k *c.dx(0).dx(0) + u*c.dx(0), u * v.dx(0)) * dx
        L += tau * inner(f, u * v.dx(0)) * dx

    # Solve problem
    C = Function(V)
    solve(A == L, C, bcs)

    c_exact_interpolated = interpolate(c_exact, V)

    #  Append norm
    return C, c_exact, c_exact_interpolated


def solutionForSimple_test_problem(SUPG, mesh, p, h, k, u) :

    V = FunctionSpace(mesh, 'CG', 2)

    m     = sqrt(2)
    Pe    = float(m * h / (2.0 * k))
    omega = coth(Pe) - Pe**(-1)
    tau   = h / 2.0 * omega

    c_exact = Expression(" sin(pi*x[0]) * cos(2*pi*x[0]) ", degree = p + 3)


    # Boundary conditions
    bcs = DirichletBC(V, c_exact, "on_boundary")

    # Define our expressions used for the variational formulaton
    tau = Constant(tau)
    k   = Constant(k)

    # f = Expression(" - diff  * pow(pi, 2) / 2 * ( sin(pi * x[0]) - 9 * sin(3 * pi * x[0]) ) " \
    #                " - pi / 2 * ( cos(pi * x[0]) - 3 * cos(3 * pi * x[0]) ) ", diff = k,  degree = p)              # Calculated by hand

    f = Expression(" diff  * pow(pi, 2) * ( 5*sin(pi*x[0])*cos(2*pi*x[0]) + 4*cos(pi*x[0])*sin(2*pi*x[0]) ) " \
                   " + pi * ( cos(pi * x[0])*cos(2*pi*x[0]) - 2 * sin(pi*x[0])*sin(2 * pi * x[0]) ) ", diff = k,  degree = p)

    #f = Expression(" diff  * pow(pi, 2) * ( 5*sin(pi*x[0])*cos(2*pi*x[0]) + 4*cos(pi*x[0])*sin(2*pi*x[0]) ) ", diff = k,  degree = p)

    c = TrialFunction(V)
    v = TestFunction(V)

    A = k * dot(grad(c), grad(v)) * dx + inner(dot(u, grad(c)), v) * dx
    L = inner(f, v) * dx
    # L = f * v * dx

    if SUPG :

        A += tau * inner(-k * div(grad(c)) + dot(u, grad(c)), dot(u, grad(v))) * dx
        L += tau * inner(f, dot(u, grad(v))) * dx

    # Solve problem
    C = Function(V)
    solve(A == L, C, bcs)

    c_exact_interpolated = interpolate(c_exact, V)

    return C, c_exact, c_exact_interpolated
