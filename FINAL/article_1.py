"""
    This is based on the article:
    Numerical Solution of the 1D Advection-Diffusion Equation
    Using Standard and Nonstandard Finite Difference Schemes.

    In the article the following is considered:
    u_t + 1.0 * u_x = 0.01 u_xx
    with 0 \le x \le 1 and time 0 \le t \le 1.
"""

from fenics import *

import ad_generator as AD_GEN
import mesh_generator as MESH_GEN
import stokes_generator as STOKES_GEN

num_polygons = 50
mesh = MESH_GEN.gen_article1_mesh(num_polygons) # generates the 1D mesh as in article

# No stokes flow was given, here we have constant velocity = 1.0

SUPG = False # Change this for different effects


# determine where you want to save the file
ad_filePath = "../../solution/article_1_fix_t.pvd"

# Just because we might want to compare the SUPG solution next to the other solution.
"""
if SUPG:
    ad_filePath += "Article_1_solution_SUPG.pvd"
else:
    ad_filePath += "Article_1_solution_SUPG.pvd"
"""
AD_GEN.solutionForArticle1(SUPG, mesh, ad_filePath)
