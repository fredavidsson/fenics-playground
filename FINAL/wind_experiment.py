import dolfin as dolfin
import mshr as mshr
import random

L = 50  # 50 mm
H = [24, 48, 72, 96]  # different heights in mm
margin = -34.09


def getRandomHeight():
    # returns a random height
    return random.choice(H)


def getRandomHouse(x, y):
    # generates a mesh house with random height by given coordinates
    X_1 = dolfin.Point(x, y, 0)
    X_2 = dolfin.Point(abs(x - L), abs(y - L), getRandomHeight())
    house = mshr.Box(X_1, X_2)
    return house


def getHouse(x, y, height):
    # generates a mesh house with given height and coordinates
    X_1 = dolfin.Point(x, y, 0)
    X_2 = dolfin.Point(abs(x - L), abs(y - L), height)
    house = mshr.Box(X_1, X_2)
    return house


houses = []

for i in range(1, 20):
    even = i % 2 == 0

    j = 6 if even else 7
    j_0 = 1 if even else 0
    for k in range(j_0 + 1, j + 1 + j_0):
        x = i * L * 2 + margin
        y = k * L * 2 - L * j_0 + margin
        houses.append(getHouse(x - 5, y - 5, height=72))
        # houses.append(getRandomHouse(x - 5, y - 5))

x_max = 1900 + margin
y_max = 700 + margin

min = dolfin.Point(0, 0, 0.0)
max = dolfin.Point(x_max, y_max, 215)
box = mshr.Box(min, max)

plane_area_ratio = (124 * L ** 2) / (x_max * y_max)

print(plane_area_ratio)

domain = houses[0]
for i in range(1, len(houses)):
    domain += houses[i]

domain = box - domain
# domain = domain - box

mesh = mshr.generate_mesh(domain, 64)
dolfin.XDMFFile('wind_mesh.xdmf').write(mesh)
