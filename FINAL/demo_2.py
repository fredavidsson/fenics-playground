from fenics import *

import ad_generator as AD_GEN
import mesh_generator as MESH_GEN
import stokes_generator as STOKES_GEN

num_polygons = 8

mesh = MESH_GEN.generate3DMesh(num_polygons) # generates the 3D mesh.

# Now generate the stokes flow for given mesh
stokes_filePath = "../../solution/STOKES_VELOCITY_3Dsolution.xdmf"
u, p = STOKES_GEN.generate3Dflow(mesh) # u is the flow velocity and p is not used in this case.

# Save the velocity flow for later use..
XDMFFile(stokes_filePath).write(u)

# use above result to solve the AD equations
SUPG = True # Change this if you want to use SUPG terms.

# determine where you want to save the file
ad_filePath = "../../solution/"

# Just because we might want to compare the SUPG solution next to the other solution.
if SUPG:
    ad_filePath += "AD_3D_solution_SUPG.pvd"
else:
    ad_filePath += "AD_3D_soluton.pvd"

AD_GEN.solutionFor3D(SUPG, mesh, ad_filePath, u)
