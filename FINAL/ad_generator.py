from fenics import inner, div, grad, dot, dx
import fenics as fenics
import numpy as np


def coth(x):
    # sympy causes stack overflow, so we use this instead.
    return np.cosh(x) / np.sinh(x)


def stabilisationParameter(u, h, k):
    """ Returns the stabilisation parameter tau, required for SUPG.

    Keyword arguments:
    u -- Velocity
    k -- Diffusive constant
    h -- The mesh step size
    """

    m = 1.0

    if isinstance(u, float):
        m = u
    elif isinstance(u, tuple):
        u = fenics.Expression(u, degree=1)
        m = fenics.norm(u, 'l2')
    else:
        m = np.sqrt( u[0] ** 2 + u[1] ** 2)


    Pe = float(m * h / (2.0 * k))
    omega = coth(Pe) - Pe ** (-1)
    tau = h / (2.0 * m) * omega

    print(m)
    print(Pe)

    return tau


def solveAD(SUPG, V, k, u, f, h, bcs):
    """ Solves the advection-diffusive equations in 2 and 3 dimensions.
    Returns A, L where A is the right-hand and L is the left-hand side of
    the AD equations.

    Keyword arguments:
    SUPG -- This will solve the Advection-Diffusive equations using
            the SUPG stabilisation parameters.

    V -- The function space
    k -- Diffusive constant
    u -- Velocity, can be an Expression or Constant
    f -- Source term, can be an Expression or Constant
    h -- The mesh step size
    """

    c = fenics.TrialFunction(V)
    v = fenics.TestFunction(V)

    A = k * dot(grad(c), grad(v)) * dx + inner(dot(u, grad(c)), v) * dx
    L = inner(f, v) * dx

    if SUPG:

        tau = stabilisationParameter(u, h, k)

        A += tau * inner(-k * div(grad(c)) + dot(u, grad(c)), dot(u, grad(v))) * dx
        L += tau * inner(f, dot(u, grad(v))) * dx

    C = fenics.Function(V)
    fenics.solve(A == L, C, bcs)

    return C


def solveAD1D(SUPG, V, k, u, f, h, bcs):
    """ Solves the advection-diffusive equations in one dimension.

    Returns the numerical solution to the problem.

    Keyword arguments:
    SUPG -- This will solve the Advection-Diffusive equations using
            the SUPG stabilisation parameters.

    V -- The function space
    k -- Diffusive constant
    u -- Velocity, can be an Expression or Constant
    f -- Source term, can be an Expression or Constant
    h -- The mesh step size
    bcs --- Boundary conditions
    """

    c = fenics.TrialFunction(V)
    v = fenics.TestFunction(V)

    A = k * inner(c.dx(0), v.dx(0)) * dx + inner(u * c.dx(0), v) * dx
    L = inner(f, v) * dx

    if SUPG is True:

        tau = stabilisationParameter(u, h, k)

        A += tau * inner(-k * c.dx(0).dx(0) + u*c.dx(0), u * v.dx(0)) * dx
        L += tau * inner(f, u * v.dx(0)) * dx

    C = fenics.Function(V)
    fenics.solve(A == L, C, bcs)

    return C
