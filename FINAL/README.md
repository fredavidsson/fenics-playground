## Time dependent AD equations
![header](supg_vs_nonsupg.jpeg)

In this picture we see a comparison on the mesh behaviour. Left side is with SUPG term and the right one is without SUPG.

To see an animation (not same solution as in this demo) check this [youtube-link here](https://www.youtube.com/watch?v=sH8zOQUBkdE).

## How to
In order to run the simulation, make sure fenics is installed. To run this demo, just run following command:

`python3 demo_1.py`
and it should give us a time dependent 2D solution (run `python3 demo_2.py` for 3D solution).

Check `demo_1.py` for more information on where the solutions are saved. Import the `.svd` files to paraview later and play the demonstration.

Note in `demo_1.py` that we save the stokes flow in the same path.

## More
Different results from some other articles has been added.

## Todo
- [x] Implement time-dependent 3D case too.
- [x] Visualize it and put a nice picture here.
