import fenics as fenics
import ad_generator as AD_GEN
import numpy as np
import matplotlib.pyplot as plt

from matplotlib import rc

# Only display one certain type of error
WARNING = 30
fenics.set_log_level(WARNING)


def coth(x):
    return np.cosh(x) / np.sinh(x)


k = 0.015
u = 1.0

fig, ax = plt.subplots()
color = ['#f34458', '#9e44f3', '#44adf3', 'k', '#f3a744', '#47c50d']

intervals = [int(8 * np.sqrt(2) ** i) for i in range(30)]

print(intervals)

for p in range(1, 2):
    print('################################')
    print("p = %d" % p)

    for SUPG in [True, False]:
        MESH_SIZE = []
        NORM_VALUES = []

        for i in intervals:
            mesh = fenics.UnitIntervalMesh(i)
            h = mesh.hmax()

            V = fenics.FunctionSpace(mesh, 'CG', p)

            f = fenics.Expression("1.0", degree=p)
            c_exact = \
                fenics.Expression("1.0 / u *( x[0] - (1 - exp(u * x[0] / k))"
                                  " / (1 - exp(u/k)) )", u=u, k=k, degree=p+3)

            bcs = fenics.DirichletBC(V, 0.0, "on_boundary")

            C = AD_GEN.solveAD1D(SUPG, V, k, u, f, h, bcs)

            L2_error = fenics.errornorm(c_exact, C)

            MESH_SIZE.append(h)
            NORM_VALUES.append(L2_error)

        x_length = len(MESH_SIZE)

        label = "Polynomial deg. %d with SUPG" if SUPG is True \
                else "Polynomial deg. %d without SUPG"

        label = label % p
        marker = "-d" if SUPG else "--x"
        ax.loglog(MESH_SIZE, NORM_VALUES, marker,
                  color=color[p - 2], label=label)

#            for h_x in np.arange(0 + h, 1 + h, h):

        if SUPG is False:
            poly = \
                np.polyfit(
                    np.log(MESH_SIZE[9:17]),
                    np.log(NORM_VALUES[9:17]), 1)
            print(poly[0])


plt.rc('font', family="sans-serif")
rc('text', usetex=True)
fig.suptitle(
    "Article test case with exact \n"
    r"$$c = \frac{1}{u} \big( x - \frac{ 1 - e^{u/\kappa} }{1 -e^{u/\kappa}} \big)$$")

ax.set(
    xlabel=r'$\log$(Mesh size)',
    ylabel=r'$\log$(L2 error)')

ax.legend()
plt.grid()
plt.show()
