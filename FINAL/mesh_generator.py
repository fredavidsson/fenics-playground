"""
    This file generates and loads specific meshes

"""

from fenics import *
from mshr import *

def gen_article1_mesh(num_x):
    """
        See article_1.py
    """
    return UnitIntervalMesh(num_x)

def gen_article2_mesh(num_x):
    """
        See file article_2_example1.py
    """
    return UnitSquareMesh(num_x, num_x)
    #UnitSquareMesh(num_polygons, num_polygons)

def generate2DMesh(num_x):
    """
        This generates our mesh in 2D case. Change here if you want different mesh
        or create a new function.
    """
    xmin = 0.0; ymin = 0.0
    xmax = 1.0; ymax = 1.0

    house_x = [xmax / 3.0, xmax - xmax / 3.0]
    house_y = [ymin, ymax / 4.0]

    house = Rectangle(Point(house_x[0], house_y[0]), Point(house_x[1], house_y[1]))
    domain = Rectangle(Point(xmin, ymin), Point(xmax, ymax)) - house

    mesh = generate_mesh(domain, num_x)

    return mesh
    #File(filePath) << mesh


def generate3DMesh(num_x):
    """
        This generates our mesh in 3D case. Change here if you want different mesh
        or create a new function.
    """
    mesh = UnitCubeMesh(num_x, num_x, num_x)
    return mesh
    #File(filePath) << mesh
