from fenics import XDMFFile
from fenics import Mesh

import ad_generator as AD_GEN
import mesh_generator as MESH_GEN
import stokes_generator as STOKES_GEN

num_polygons = 32 # Number of polygons of given domain

mesh_filePath = "../../solution/mesh2D.xml"
MESH_GEN.generate2DMesh(mesh_filePath, num_polygons) # generates and saves the 2D mesh. Note that this only needs to be runned once. 

mesh = Mesh(mesh_filePath)

# Now generate the stokes flow for given mesh
stokes_filePath = "../../solution/STOKES_VELOCITY_2Dsolution.xdmf"
u, p = STOKES_GEN.generate2Dflow(mesh) # u is the flow velocity and p is not used in this case. 

# Save the velocity flow for later use.. 
XDMFFile(stokes_filePath).write(u)

# use above result to solve the AD equations
SUPG = True # Change this if you want to use SUPG terms.

# determine where you want to save the file
ad_filePath = "../../solution/"

# Just because we might want to compare the SUPG solution next to the other solution. 
if SUPG:
    ad_filePath += "AD_2D_solution_SUPG.pvd" 
else:
    ad_filePath += "AD_2D_soluton.pvd"

AD_GEN.solutionFor2D(SUPG, mesh, ad_filePath, u)