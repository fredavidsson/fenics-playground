from mshr import *
from fenics import *
from mpmath import coth
from mpmath import tanh
from math import pi
import ad_generator as AD_GEN
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc               # for Latex

"""
    Example 4.2 from articel 'Residual - based stabilized higher - order FEM
    for advection - dominated problems' by G.Lube and G.Rapin


    Code tailor customized for polynomial comparison

"""

# ------ The advection-diffusive (AD) parameters ------  #

k = 10**(-16)  # Kappa (diffusive constant)

# Velocity constant
u = [1.0  / sqrt(5), 2.0 / sqrt(5)]


# Only display one certain type of error
WARNING = 30
set_log_level(WARNING)

# Generate mesh
mesh  = UnitSquareMesh(10, 10)
h     = mesh.hmax()

C_SUPG, c_exact, c_exact_interpolated = AD_GEN.solutionForArticle2Example2(True, mesh, 3, h, k, u)
C_noSUPG, _, _ = AD_GEN.solutionForArticle2Example2(False, mesh, 3, h, k, u)

def writeFile(filePath, solution):
    file = File(filePath)
    file << solution

writeFile('article2_ex_42_exact.pvd', c_exact_interpolated)
writeFile('article2_ex_42_SUPG.pvd', C_SUPG)
writeFile('article2_ex_42_noSUPG.pvd', C_noSUPG)
