from mshr import *
from fenics import *
from mpmath import coth
from math import pi
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D  # Necessary for 3D plotting
from matplotlib import cm
from matplotlib.ticker import NullFormatter # Useful for 'logit' scale
import ad_generator as AD_GEN



"""
    A simple test problem to verity that the code give good convergence
    for a stable problem

    SUPG: Can be given True or False
"""

# The advection-diffusive (AD) equations

k = 10**(-1)  # Kappa (diffusive constant)

# This is the velocity b given by Logg
u = Constant( (1.0, 1.0) )


# Only display one certain type of error
WARNING = 30
set_log_level(WARNING)

# fig, (ax1, ax2) = plt.subplots(1, 2, sharex = False, sharey = False)
fig, ax         = plt.subplots()

SUPG_state      = [False]
color           = ['m', 'g', 'r', 'k', 'c']


for poly_degree in range(2, 7) :

    SUPG_norm_values = []
    norm_values      = []


    print(' ')
    print('############################################ ')
    print('poly_degree = %d' % poly_degree)

    for SUPG in SUPG_state :

        mesh_size = []

        for i in range(1, 7) :

            # Mesh parameters
            triangles_nr = 2 ** i
            mesh         = UnitSquareMesh(triangles_nr, triangles_nr)
            h            = triangles_nr ** (-1)

            mesh_size.append(h)

            C, c_exact, _ = AD_GEN.solutionForSimple_test_problem(SUPG, mesh, poly_degree, h, k, u)

            L2_norm = errornorm(c_exact, C)

            if SUPG:
                SUPG_norm_values.append(L2_norm)
            else:
                norm_values.append(L2_norm)

    # label = 'Polynomial degree %d' % poly_degree
    # ax1.loglog(mesh_size, SUPG_norm_values, '--o', label = label)
    # ax2.loglog(mesh_size, norm_values, '--s', label = label)

    label_1 = 'Polynom degree %d with SUPG' % poly_degree
    label_2 = 'Polynom degree %d without SUPG' % poly_degree

    #ax.loglog(mesh_size, SUPG_norm_values, '--o', color = color[poly_degree - 2], label = label_1)
    ax.plot(mesh_size, norm_values, '-s',  color = color[poly_degree - 2], label = label_2)
    # ax.loglog

    # ------- Fit and print each polynomial degree ------- #
    # polyfit_SUPG_coef = np.polyfit(np.log(mesh_size), np.log(SUPG_norm_values), 1)
    # polyfit_noSUPG_coef = np.polyfit(mesh_size, norm_values, 1)
    # print('Polyfit at degree', poly_degree, '=', polyfit_SUPG_coef[0])





# # Create head title
# fig.suptitle('Simple test case with c_exact = sin(pi*x) * cos(2*pi*x)')
# # Sub plots label
# ax1.set_title('With SUPG')
# ax2.set_title('Without SUPG')

ax.set_title('Simple test case with c_exact = sin(pi*x) * cos(2*pi*x)')


# ax1.set(xlabel = 'log(Mesh size)', ylabel = 'log(L2 error)')
# ax2.set(xlabel = 'log(Mesh size)', ylabel = 'log(L2 error)')
ax.set(xlabel = 'log(Mesh size)', ylabel = 'log(L2 error)')


# ax1.legend()
# ax2.legend()
ax.legend()
plt.show()


# # Solve again to save files for Paraview
# triangles_nr = 32
# mesh         = UnitSquareMesh(triangles_nr, triangles_nr)
# h            = 1.0 / triangles_nr
#
# poly_degree = 2
# C_SUPG, c_exact, c_exact_interpolated = AD_GEN.solutionForSimple_test_problem(True, mesh, poly_degree, h, k, u)
#
# C_noSUPG, _, _ = AD_GEN.solutionForSimple_test_problem(False, mesh, poly_degree, h, k, u)
#
#
# # ------------------------ Save the file ------------------------ #
#
# savePath_exact = '/home/darebro/Documents/Master_Thesis/Results_playground/simple_test_problem_solution_exact.pvd'
# pvdFile_exact  = File(savePath_exact)
#
# savePath_SUPG  = '/home/darebro/Documents/Master_Thesis/Results_playground/simple_test_problem_solution_SUPG.pvd'
# pvdFile_SUPG   = File(savePath_SUPG)
#
# savePath_noSUPG  = '/home/darebro/Documents/Master_Thesis/Results_playground/simple_test_problem_solution_noSUPG.pvd'
# pvdFile_noSUPG   = File(savePath_noSUPG)
#
# pvdFile_exact << c_exact_interpolated
# pvdFile_SUPG << C_SUPG
# pvdFile_noSUPG << C_noSUPG
#
















#
