import fenics as fenics

import ad_generator as AD_GEN
import mesh_generator as MESH_GEN
import stokes_generator as STOKES_GEN

num_polygons = 32  # Number of polygons of given domain

mesh = MESH_GEN.generate2DMesh(num_polygons) # generates the 2D mesh.

# Now generate the stokes flow for given mesh
stokes_filePath = "../../solution/STOKES_VELOCITY_2Dsolution.xdmf"
u, p = STOKES_GEN.generate2Dflow(mesh)  # u is the flow velocity and p is not used in this case.

# Save the velocity flow for later use..
fenics.XDMFFile(stokes_filePath).write(u)

# use above result to solve the AD equations
SUPG = True  # Change this if you want to use SUPG terms.

k = 0.00001  # Kappa (diffusive constant)
num_steps = 500  # Time steps
T = 5  # End time
dt = T / num_steps  # Delta time

h = mesh.hmax()
V = fenics.FunctionSpace(mesh, 'CG', 1)
bcs = fenics.DirichletBC(V, 0.0, "near(x[0], 0.0)")
f = fenics.Expression(
    'A * exp(-B * ( pow(x[0] - 0.1, 2) + pow(x[1] - 0.1, 2) )) < 0.0001 ? 0.0 : 0.1',
    A=0.005, B=1000.0, degree=2)


AD_GEN.solveAD(SUPG, V, k, u, f, h, bcs)


# determine where you want to save the file
ad_filePath = "../../solution/"

# Just because we might want to compare the SUPG solution next to the other solution.
if SUPG:
    ad_filePath += "AD_2D_solution_SUPG.pvd"
else:
    ad_filePath += "AD_2D_soluton.pvd"


# AD_GEN.solutionFor2D(SUPG, mesh, ad_filePath, u)
