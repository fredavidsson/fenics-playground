import csv
from scipy.interpolate import interp2d

# Import CSV file
filePath = "../vel.csv"

x,y,v = [], [], []

with open(filePath, 'r', newline='') as file:
    has_header = csv.Sniffer().has_header(file.read(1024))
    file.seek(0) # rewind
    reader = csv.reader(file)
    
    # Skip header, because the very first row in the file is a string.
    if has_header:
        next(reader)

    for row in reader:
        x.append( float(row[1]) ) # Convert to float because interp2d dont like strings
        y.append( float(row[2]) )
        v.append( float(row[4]) )


print("Interpolating data.. ")

interpolant = interp2d(x, y, v, kind='linear', copy=False, bounds_error=True)

print("done")