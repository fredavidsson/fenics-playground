## Navier-Stokes Equations
This is a 2D solution with values exported from IBOFlow.

## Todo

- [ ] Implement the data points in the .txt file from main folder: `fenics-playground/`.
- [ ] Re-write it in a compact form just as in the sub-project `fenics-playground/FINAL`  folder. 

 